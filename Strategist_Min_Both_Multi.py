import random
import sys
import os
import pandas as pd
import numpy as np
import math
from datetime import datetime, timedelta, tzinfo, timedelta

NONE = 4
BUY_ASKING = 2
SELL_ASKING = 5
BUY_ON_INTEREST = 3
SELL_ON_INTEREST = 6
BUY_CLOSING = 7
SELL_CLOSING = 8
BUY_REVERSE = 9
SELL_REVERSE = 10

WIN = 1
LOSS = 0

DIR_BUY = 1
DIR_SELL = 2

class Strategist:
    def __init__(self):

        self.buy_stop_loss = []
        self.buy_stop_profit = []
        self.buy_move_loss = []
        self.buy_do_stop_profit = []
        self.buy_close_new_thres = []
        self.buy_kd_thres = []
        self.buy_kd_thres_upper = []
        self.buy_close_new_thres_upper = []
        self.buy_bolling_rz = []

        self.sell_stop_loss = []
        self.sell_stop_profit = []
        self.sell_move_loss = []
        self.sell_do_stop_profit = []
        self.sell_close_new_thres = []
        self.sell_kd_thres = []
        self.sell_kd_thres_upper = []
        self.sell_close_new_thres_upper = []
        self.sell_bolling_rz = []

        self.buy_total_profit = 0
        self.buy_win_ratio = 0
        self.buy_win_count = 0
        self.buy_trade_count = 0
        self.buy_real_profit = 0
        self.buy_win_profit = 0
        self.buy_loss_profit = 0

        self.sell_total_profit = 0
        self.sell_win_ratio = 0
        self.sell_win_count = 0
        self.sell_trade_count = 0
        self.sell_real_profit = 0
        self.sell_win_profit = 0
        self.sell_loss_profit = 0

        self.highest = 0
        self.lowest = 0
        self.static_close = 0

        self.last_highest = 0
        self.last_lowest = 0
        self.last_static_close = 0

        self.status = NONE
        self.new_price = 0
        self.std_price = 0
        self.stop_loss_price = 0

        self.tick_profit = sys.maxsize

        self.H_line = self.M_line = self.L_line = 0

        self.buy_KLine_upper_rz = []
        self.buy_KLine_body_rz = []
        self.buy_KLine_low_rz = []
        self.buy_KLine_trade = []

        self.sell_KLine_upper_rz = []
        self.sell_KLine_body_rz = []
        self.sell_KLine_low_rz = []
        self.sell_KLine_trade = []

        self.strategist_num = 0
        self.strategist_direct = []

        self.daily_profit = 0


    def add_strategist(self, stop_loss, stop_profit, move_loss, do_stop_profit, \
                    KLine_low_rz, KLine_body_rz, KLine_upper_rz, KLine_trade, buy_or_sell):

        close_new_thres = bolling_rz = buy_close_new_thres = kd_thres = 0

        self.buy_stop_loss.append(stop_loss)
        self.buy_stop_profit.append(stop_profit)
        self.buy_move_loss.append(move_loss)
        self.buy_do_stop_profit.append(do_stop_profit)
        self.buy_close_new_thres.append(buy_close_new_thres)
        self.buy_close_new_thres_upper.append(buy_close_new_thres + 10)
        self.buy_kd_thres.append(kd_thres)
        self.buy_kd_thres_upper.append(kd_thres + 10)
        self.buy_bolling_rz.append(bolling_rz)
        self.buy_KLine_low_rz.append(KLine_low_rz)
        self.buy_KLine_body_rz.append(KLine_body_rz)
        self.buy_KLine_upper_rz.append(KLine_upper_rz)
        self.buy_KLine_trade.append(KLine_trade)

        self.sell_stop_loss.append(stop_loss)
        self.sell_stop_profit.append(stop_profit)
        self.sell_move_loss.append(move_loss)
        self.sell_do_stop_profit.append(do_stop_profit)
        self.sell_close_new_thres.append(buy_close_new_thres)
        self.sell_close_new_thres_upper.append(buy_close_new_thres + 10)
        self.sell_kd_thres.append(kd_thres)
        self.sell_kd_thres_upper.append(kd_thres + 10)
        self.sell_bolling_rz.append(bolling_rz)
        self.sell_KLine_low_rz.append(KLine_low_rz)
        self.sell_KLine_body_rz.append(KLine_body_rz)
        self.sell_KLine_upper_rz.append(KLine_upper_rz)
        self.sell_KLine_trade.append(KLine_trade)

        self.strategist_direct.append(buy_or_sell)

        self.strategist_num += 1 
        self.strategist_select = 0

    def close_deal(self, close):

        if (BUY_CLOSING == self.status):
            self.tick_profit = close - self.new_price
            if (self.tick_profit > 0):
                self.buy_win_count += 1
                self.buy_win_profit += self.tick_profit - 1
            else:
                self.buy_loss_profit += self.tick_profit - 1

            self.buy_total_profit += self.tick_profit
            self.status = NONE
            print ('price:', close, ', profit:', self.tick_profit, ', buy_total_profit:', self.buy_total_profit)

        elif (SELL_CLOSING == self.status):
            self.tick_profit = self.new_price - close
            if (self.tick_profit > 0):
                self.sell_win_count += 1
                self.sell_win_profit += self.tick_profit - 1
            else:
                self.sell_loss_profit += self.tick_profit - 1

            self.sell_total_profit += self.tick_profit
            self.status = NONE
            print ('price:', close, ', profit:', self.tick_profit, ', sell_total_profit:', self.sell_total_profit)

    def price_to_rz(self, price):
        start_idx = 5
        idx_jump = 10

        for idx in range(0, 5, 1):
            if (price < idx * idx_jump + start_idx):
                return idx

        return 5


    def check_new_condition(self):

        if (self.static_close >= self.last_static_close):
            low_line = self.last_static_close - self.lowest
            body_line = self.static_close - self.last_static_close
            upper_line = self.highest - self.static_close
            trade = 1
        else:
            low_line = self.static_close - self.lowest
            body_line = self.last_static_close - self.static_close
            upper_line = self.highest - self.last_static_close
            trade = 0

        for index in range(0, self.strategist_num, 1):
            if (DIR_BUY == self.strategist_direct[index]):
                if (trade == self.buy_KLine_trade[index] and self.buy_KLine_low_rz[index] == self.price_to_rz(low_line) and \
                    self.buy_KLine_body_rz[index] == self.price_to_rz(body_line) and self.buy_KLine_upper_rz[index] == self.price_to_rz(upper_line)):

                    print ('low_line:', low_line, ', body_line:', body_line, 'upper_line:', upper_line, 'trade:', trade, ', chose:', index)

                    return index

            else:
                if (trade == self.sell_KLine_trade[index] and self.sell_KLine_low_rz[index] == self.price_to_rz(low_line) and \
                    self.sell_KLine_body_rz[index] == self.price_to_rz(body_line) and self.sell_KLine_upper_rz[index] == self.price_to_rz(upper_line)):

                    print ('low_line:', low_line, ', body_line:', body_line, 'upper_line:', upper_line, 'trade:', trade, ', chose:', index)
                    
                    return index

        return None


    def handle_with_static_KLine(self, close, high, low, slowk, slowd, ema_fast, ema_middle, ema_slow, H_line, M_line, L_line):

        self.highest = high
        self.lowest = low
        self.static_close = close

        if (self.last_static_close != 0):

            new_index = self.check_new_condition()

            if (None != new_index):
                if (NONE == self.status):
                    if (DIR_BUY == self.strategist_direct[new_index]):
                        self.status = BUY_ASKING                        
                    else:
                        self.status = SELL_ASKING

                    self.strategist_select = new_index

                elif (BUY_ON_INTEREST == self.status):
                    if (DIR_BUY == self.strategist_direct[new_index] and new_index < self.strategist_select):
                        self.stop_loss_price = close - self.buy_stop_loss[new_index]
                        self.std_price = close
                        print ('change buy strategist:', self.strategist_select, ' to ', new_index, ', stop_loss:', self.stop_loss_price)
                        self.strategist_select = new_index

                    elif (DIR_SELL == self.strategist_direct[new_index]):
                        self.status = BUY_REVERSE
                        print ('reverse buy strategist:', self.strategist_select, ' to ', new_index)
                        self.strategist_select = new_index

                elif (SELL_ON_INTEREST == self.status):
                    if (DIR_SELL == self.strategist_direct[new_index] and new_index < self.strategist_select):
                        self.stop_loss_price = close + self.sell_stop_loss[new_index]
                        self.std_price = close
                        print ('change sell strategist:', self.strategist_select, ' to ', new_index, ', stop_loss:', self.stop_loss_price)
                        self.strategist_select = new_index

                    elif (DIR_BUY == self.strategist_direct[new_index]):
                        self.status = SELL_REVERSE
                        print ('reverse sell strategist:', self.strategist_select, ' to ', new_index)
                        self.strategist_select = new_index

        if (1 == self.buy_move_loss[self.strategist_select] and BUY_ON_INTEREST == self.status):
            if (high - self.buy_stop_loss[self.strategist_select] > self.stop_loss_price):
                self.stop_loss_price = high - self.buy_stop_loss[self.strategist_select]

        elif (1 == self.sell_move_loss[self.strategist_select] and SELL_ON_INTEREST == self.status):
            if (low + self.sell_stop_loss[self.strategist_select] < self.stop_loss_price):
                self.stop_loss_price = low + self.sell_stop_loss[self.strategist_select]

        self.last_highest = self.highest
        self.last_lowest = self.lowest
        self.last_static_close = self.static_close

    def handle_with_tick(self, ts, close):

        if (BUY_ASKING == self.status):
            self.new_price = close
            self.std_price = close
            self.stop_loss_price = close - self.buy_stop_loss[self.strategist_select]
            self.buy_trade_count += 1

            print ('buy new:', close, ', stop_loss:', self.stop_loss_price)
            self.status = BUY_ON_INTEREST

        elif (SELL_ASKING == self.status):
            self.new_price = close
            self.std_price = close
            self.stop_loss_price = close + self.sell_stop_loss[self.strategist_select]
            self.sell_trade_count += 1

            print ('sell new:', close, ', stop_loss:', self.stop_loss_price)
            self.status = SELL_ON_INTEREST

        elif (BUY_REVERSE == self.status):
            self.status = BUY_CLOSING
            self.close_deal(close)
            self.status = SELL_ASKING

        elif (SELL_REVERSE == self.status):
            self.status = SELL_CLOSING
            self.close_deal(close)
            self.status = BUY_ASKING

        elif (BUY_ON_INTEREST == self.status):

            if (close < self.stop_loss_price):
                self.status = BUY_CLOSING
                print ('[buy stop loss]')

            elif (close > self.std_price + self.buy_stop_profit[self.strategist_select] and 1 == self.buy_do_stop_profit[self.strategist_select]):
                self.status = BUY_CLOSING
                print ('[buy stop profit]')

        elif (SELL_ON_INTEREST == self.status):

            if (close > self.stop_loss_price):
                self.status = SELL_CLOSING
                print ('[sell stop loss]')

            elif (close < self.std_price - self.sell_stop_profit[self.strategist_select] and 1 == self.sell_do_stop_profit[self.strategist_select]):
                self.status = SELL_CLOSING
                print ('[sell stop profit]')

        elif (BUY_CLOSING == self.status or SELL_CLOSING == self.status):
            self.close_deal(close)

            
    def force_close(self, tick_price):

        if (BUY_ON_INTEREST == self.status):
            self.status = BUY_CLOSING
            print ('[buy force close]')
            self.close_deal(tick_price)
            
        if (SELL_ON_INTEREST == self.status):
            self.status = SELL_CLOSING
            print ('[sell force close]')
            self.close_deal(tick_price)

        self.highest = 0
        self.lowest = 0
        self.static_close = 0

        self.last_highest = 0
        self.last_lowest = 0
        self.last_static_close = 0

        self.status = NONE
            
        
    def settlement(self):
        if (self.buy_trade_count != 0):
            self.buy_win_ratio = self.buy_win_count/self.buy_trade_count
        self.buy_real_profit = self.buy_total_profit - self.buy_trade_count

        if (self.sell_trade_count != 0):
            self.sell_win_ratio = self.sell_win_count/self.sell_trade_count
        self.sell_real_profit = self.sell_total_profit - self.sell_trade_count

    def report(self):
        attrs = vars(self)
        print(', '.join("%s: %s" % item for item in attrs.items()))

    def get_info(self):
        return self.buy_total_profit, self.buy_trade_count, self.buy_win_count, self.buy_win_ratio, self.buy_real_profit, self.buy_win_profit, self.buy_loss_profit, \
            self.sell_total_profit, self.sell_trade_count, self.sell_win_count, self.sell_win_ratio, self.sell_real_profit, self.sell_win_profit, self.sell_loss_profit

    def refresh_tick_profit(self):
        if (sys.maxsize != self.tick_profit):
            profit = self.tick_profit - 1
            self.daily_profit += profit
        else:
            profit = sys.maxsize
        
        self.tick_profit = sys.maxsize

        return profit

    def get_daily_profit(self):
        daily_profit = self.daily_profit
        self.daily_profit = 0
        return daily_profit

    def clear_states(self):
        self.status = NONE

    def get_status(self):
        return self.status
        
