import pandas as pd
import sys
import os
import numpy as np
import math
parent_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.sys.path.insert(0, parent_dir)
from datetime import datetime, timedelta, tzinfo, timedelta
from dateutil.relativedelta import relativedelta

# import Strategist_Min_Sep
# import Strategist_Min_Both
# import Strategist_Multi
import talib
import Static_Min_Tim
# import Strategist_Tim
# import Strategist_Enzo


class Profit_Counter:
    def __init__(self, period):

        self.period = period

        self.win_profit = []
        self.loss_profit = []
        
        self.start_date = self.end_date = 0

        self.profit_vector = []
        
        self.first = 1


    def static_data(self):
        
        profit_vector = self.win_profit / self.loss_profit.replace(0, 1)

        self.profit_vector.append(profit_vector.abs())

        self.win_profit = []
        self.loss_profit = []

        self.first = 1


    def record_profit(self, date, df):

        if (0 == self.start_date):
            self.start_date = date
            self.end_date = date + timedelta(days=self.period)

        if (date >= self.end_date):
            self.static_data()
            self.end_date = date + timedelta(days=self.period)


        win_temp = df['profit'].astype('int32')
        loss_temp = df['profit'].astype('int32')

        win_temp[win_temp < 0] = 0
        loss_temp[loss_temp > 0] = 0

        if (self.first):
            self.win_profit = win_temp
            self.loss_profit = loss_temp
            self.first = 0
        else:
            self.win_profit += win_temp
            self.loss_profit += loss_temp


    def get_info(self):
        return self.profit_vector, self.period

    def clear_profit(self):

        self.total_profit = 0

        self.trade_count = 0
        self.win_count = 0
        self.loss_count = 0

        self.win_profit = 0
        self.loss_profit = 0


year = 2012

Start_Date = datetime(year, 1, 1, 0, 0)
End_Date = datetime(2021, 12, 1, 0, 0)

date_loop = Start_Date
delta = timedelta(days=1)

record_delta = 1
record_month = Start_Date.month

profit_counter = []

for period in range(10, 100, 10):
    profit_counter.append(Profit_Counter(period))

# profit_counter.append(Profit_Counter(30))

# _file_path = '../daily_trading_details/dest_reduce/'
_file_path = '../../CSVParser/daily_trading_details/dest_reduce/'


read_year = 0

while date_loop < End_Date:

    if (read_year != date_loop.year):
        df = pd.read_csv('enzo/' + str(date_loop.year) + '_static_daily_5m_trader_tim_enzo.csv', low_memory=False, memory_map=True)
        read_year = date_loop.year

    print(date_loop)

    # if (date_loop.month > record_month):
    #     for obj in profit_counter:
    #         record_result(obj)
    #         obj.clear_profit()
    #     record_month += record_delta

    filename = datetime.strftime(date_loop, '%Y%m%d') + '_TX.csv'
    if not os.path.isfile(_file_path + filename):
        date_loop += delta
        continue

    temp_df = df.loc[(df['date'] == int(datetime.strftime(date_loop, '%Y%m%d')))].reset_index()
    for obj in profit_counter:
        obj.record_profit(date_loop, temp_df)

    date_loop += delta


Result = { 'period':[], 'average_dev':[]}

for obj in profit_counter:
    r, period = obj.get_info()
    # print (r)
    dfLine = pd.DataFrame(r)
    dfLine = dfLine.T

    dev = 0
    count = 0
    for i in range (0, len(r) - 1, 1):
        temp = (r[i] * r[i+1]) * (r[i] * r[i+1])
        dev += sum(temp)/len(temp)
        count += 1

    print ('period:', period, ', dev:', dev, ', ave_dev:', dev/count)

    Result['period'].append(period)
    Result['average_dev'].append(dev/count)

    dfLine.to_csv(str(period) + '_profit.csv', index=False, encoding='utf_8_sig');    

dfLine = pd.DataFrame(Result)
dfLine.to_csv('period_dev.csv', index=False, encoding='utf_8_sig');    

# dfLine.to_csv('dennis/' + str(year) + '_simulate_profit_dennis_tradeAllow_year.csv', index=False, encoding='utf_8_sig');
# dfLine.to_csv('enzo/' + str(year) + '_simulate_profit_enzo_tradeAllow_year.csv', index=False, encoding='utf_8_sig');
# dfLine.to_csv('enzo/' + str(year) + '_simulate_profit_enzo_year_ratio.csv', index=False, encoding='utf_8_sig');
# dfLine.to_csv('enzo/' + str(year) + '_simulate_profit_dennis_year_open_diff.csv', index=False, encoding='utf_8_sig');
# dfLine.to_csv('dennis/' + str(year) + '_simulate_profit_dennis_year.csv', index=False, encoding='utf_8_sig');
# dfLine.to_csv('dennis/' + str(year) + '_simulate_profit_dennis_allhigh_year.csv', index=False, encoding='utf_8_sig');
# dfLine.to_csv('enzo/' + str(year) + '_simulate_profit_1_enzo_year_open_diff_thres.csv', index=False, encoding='utf_8_sig');


