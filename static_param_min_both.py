import pandas as pd
import sys
import os
import numpy as np
import math

from datetime import datetime, timedelta, tzinfo, timedelta, date
from dateutil.relativedelta import relativedelta

import Strategist_Min_Both_Multi
import Static_Min

# import Strategist_Min_Sep
# import Strategist_Min_Both
# import Strategist_Multi
import talib
# import Static_Min_Tim
# import Strategist_Tim
# import Strategist_Enzo

DATE = '成交日期'
CONTRACT = '商品代號'
EXPIRED_MONTH = '到期月份(週別)'
TIME = '成交時間'
PRICE = '成交價格'
VOLUME = '成交數量(B+S)'

def count_week(test_date):
    monday1 = (date(2012,1,1) - timedelta(days=date(2012,1,1).weekday()))
    monday2 = (test_date - timedelta(days=test_date.weekday()))

    return (monday2.date() - monday1).days / 7

def back_to_date(weeks):

    monday2 = date(2012, 1, 1) + timedelta(days=(weeks * 7))

    return monday2

NONE = 0
TRAINING = 1
TESTING = 2
DONE = 3
NO_BEST = 4

def daily_trade(date, filename, list, static_min_handler):

    _daily_df = pd.read_csv(_file_path + filename, low_memory=False, memory_map=True)
    print (filename)

    k_save = d_save = 0
    new_time_save = datetime.strptime("84500", "%H%M%S")
    buy_sell_save = 0

    highest = 0
    lowest = 999999
    found = ema_count = 0

    closePrice = 0
    checkNight = 0

    endTime = datetime.strptime("134500", "%H%M%S")

    # timeCheck = dayStart

    for index, row in _daily_df.iterrows():

        ts = datetime.strptime(str(row[TIME]).zfill(6), "%H%M%S")

        static_min_handler.static_tick(date, ts, row[PRICE])

        lastClose = row[PRICE]

    static_min_handler.static_min(endTime, lastClose)

    for obj in list:
        obj.force_close(lastClose)
        profit = obj.refresh_tick_profit()
        # if (profit != sys.maxsize):
        #     static_min_handler.assigned_result(date.strftime("%Y%m%d"), profit, ts.strftime("%H%M%S"))

    static_min_handler.clear_daily()



class Profit_Counter:
    def __init__(self, train_period, test_period, profit_counter_serial, profit_factor_thres):

        self.train_period = train_period
        self.test_period = test_period

        self.start_week = self.end_week = self.test_week = 0

        self.training_win_profit = []
        self.training_loss_profit = []

        self.testing_win_profit = []
        self.testing_loss_profit = []
        
        self.first = 1

        self.state = NONE

        self.best_param = -1

        self.best_param_list = []
        self.best_param_list_idx = []
        self.best_profit = 0
        self.best_win_profit = 0
        self.best_loss_profit = 0
        self.best_profit_factor = 0

        self.test_profit = 0
        self.test_win_profit = 0
        self.test_loss_profit = 0
        self.test_profit_factor = 0

        self.get_param = 0

        self.profit_counter_serial = profit_counter_serial
        self.profit_factor_thres = profit_factor_thres
        self.profit_factor_thres_upper = profit_factor_thres + 1


    def static_data(self, df):
        
        profit_vector = abs(self.training_win_profit / self.training_loss_profit.replace(0, 1))
        profit = self.training_win_profit + self.training_loss_profit
        
        static_df = pd.DataFrame(columns=['profit_factor'])

        static_df['profit_factor'] = profit_vector
        static_df['win_profit'] = self.training_win_profit
        static_df['loss_profit'] = self.training_loss_profit
        static_df['profit'] = profit

        if (10 == self.profit_factor_thres):
            temp = static_df[static_df['profit_factor'] >= self.profit_factor_thres]
        else:
            temp = static_df[static_df['profit_factor'] >= self.profit_factor_thres]
            temp = temp[temp['profit_factor'] < self.profit_factor_thres_upper]
        # temp = temp[temp['profit_factor'] <= 10]

        if (temp.empty):
            self.state = DONE
            print ('cant find param')
            return

        profit_sort = temp.sort_values(by=['profit_factor'], ascending=False)

        print (profit_sort)

        KLine_low_rz = []
        KLine_body_rz = []
        KLine_upper_rz = []
        KLine_trade = []
        buy_sell = []

        print ('profit_sort.index.size:', profit_sort.index.size)

        for idx in range(0, profit_sort.index.size, 1):
            param_idx = 0
            found = 0

            # print ('idx:', idx)

            KLine_low_rz_tmp = df.iloc[profit_sort.index[idx]]['KLine_low_rz']
            KLine_body_rz_tmp = df.iloc[profit_sort.index[idx]]['KLine_body_rz']
            KLine_upper_rz_tmp = df.iloc[profit_sort.index[idx]]['KLine_upper_rz']
            KLine_trade_tmp = df.iloc[profit_sort.index[idx]]['KLine_trade']
            buy_sell_tmp = df.iloc[profit_sort.index[idx]]['buy_sell']

            for param in KLine_low_rz:
                if (KLine_low_rz_tmp == KLine_low_rz[param_idx] and \
                    KLine_body_rz_tmp == KLine_body_rz[param_idx] and \
                    KLine_upper_rz_tmp == KLine_upper_rz[param_idx] and \
                    KLine_trade_tmp == KLine_trade[param_idx] and \
                    buy_sell_tmp == buy_sell[param_idx]):
                    found = 1
                    break

                param_idx += 1

            if (0 == found):

                best_param_list = []

                best_param_list.append(df.iloc[profit_sort.index[idx]]['stop_loss'])
                best_param_list.append(df.iloc[profit_sort.index[idx]]['stop_profit'])
                best_param_list.append(df.iloc[profit_sort.index[idx]]['move_loss'])
                best_param_list.append(df.iloc[profit_sort.index[idx]]['do_stop_profit'])
                best_param_list.append(df.iloc[profit_sort.index[idx]]['KLine_low_rz'])
                best_param_list.append(df.iloc[profit_sort.index[idx]]['KLine_body_rz'])
                best_param_list.append(df.iloc[profit_sort.index[idx]]['KLine_upper_rz'])
                best_param_list.append(df.iloc[profit_sort.index[idx]]['KLine_trade'])
                best_param_list.append(df.iloc[profit_sort.index[idx]]['buy_sell'])

                KLine_low_rz.append(KLine_low_rz_tmp)
                KLine_body_rz.append(KLine_body_rz_tmp)
                KLine_upper_rz.append(KLine_upper_rz_tmp)
                KLine_trade.append(KLine_trade_tmp)
                buy_sell.append(buy_sell_tmp)

                self.best_param_list_idx.append(profit_sort.index[idx])

                print ('add:', KLine_low_rz_tmp, KLine_body_rz_tmp, \
                    KLine_upper_rz_tmp, KLine_trade_tmp, buy_sell_tmp)

                self.best_win_profit += self.training_win_profit[profit_sort.index[idx]]
                self.best_loss_profit += self.training_loss_profit[profit_sort.index[idx]]

                self.best_profit += profit[profit_sort.index[idx]]

                self.best_param_list.append(best_param_list)


        self.best_profit_factor = -self.best_win_profit/self.best_loss_profit

        print (self.best_param_list_idx)
        print (len(self.best_param_list_idx))
        print (self.best_win_profit, self.best_loss_profit, self.best_profit_factor)

        self.state = TESTING

    def training_data(self, week_idx, df):
        
        if (week_idx > self.end_week):
            self.static_data(df)
            return

        win_temp = df['profit'].astype('int32')
        loss_temp = df['profit'].astype('int32')

        win_temp[win_temp < 0] = 0
        loss_temp[loss_temp > 0] = 0

        if (1 == self.first):
            self.training_win_profit = win_temp
            self.training_loss_profit = loss_temp
            self.first = 0
        else:
            self.training_win_profit += win_temp
            self.training_loss_profit += loss_temp


    def testing_data(self, week_idx, df):

        if (week_idx > self.test_week):
            self.state = DONE
            print ('test done')
            return

        profit = 0

        for idx in range(0, len(self.best_param_list_idx), 1):
            profit = df['profit'].iloc[self.best_param_list_idx[idx]]

            if (profit >= 0):
                self.test_win_profit += profit
            else:
                self.test_loss_profit += profit

            self.test_profit += profit



    def record_profit(self, week_idx, df):

        if (DONE == self.state):
            return

        if (NONE == self.state):
            self.start_week = week_idx
            self.end_week = self.start_week + self.train_period
            self.test_week = self.end_week + self.test_period

            print ('start:', self.start_week, ', end:', self.end_week)

            self.state = TRAINING

        if (TRAINING == self.state):
            self.training_data(week_idx, df)

        if (TESTING == self.state):
            self.testing_data(week_idx, df)



    def get_info(self):

        if (self.test_loss_profit != 0):
            self.test_profit_factor = - self.test_win_profit / self.test_loss_profit

        return self.start_week, self.end_week, self.test_week, \
            self.train_period, self.test_period, self.best_profit, self.best_win_profit, self.best_loss_profit, \
            self.best_profit_factor, self.test_profit, self.test_win_profit, self.test_loss_profit, self.test_profit_factor, \
            len(self.best_param_list_idx), self.profit_factor_thres

    def clear_profit(self):

        return

    def get_status(self):
        return self.state

    def get_train_period(self):
        get_param = self.get_param
        self.get_param = 1

        return self.train_period, get_param, self.best_param_list, self.profit_counter_serial

    def get_counter_serial(self):
        return self.profit_counter_serial


year = 2018

Start_Date = datetime(year, 1, 1, 0, 0)
End_Date = datetime(2021, 8, 15, 0, 0)

date_loop = Start_Date
delta = timedelta(days=1)

profit_counter = []
profit_counter_serial = 0

# for period in range(10, 100, 10):
#     profit_counter.append(Profit_Counter(period))

# for train_weeks in range(2, 24, 2):
for train_weeks in range(24, 70, 12):
    
    # for test_weeks in range(1, 5, 1):

    for profit_factor_thres in range(2, 11, 1):
        profit_counter.append(Profit_Counter(train_weeks, 1, profit_counter_serial, profit_factor_thres))
        profit_counter_serial += 1

    # profit_counter.append(Profit_Counter(train_weeks, 1, profit_counter_serial, 10))
    # profit_counter_serial += 1

# _file_path = '../daily_trading_details/dest_reduce/'
_file_path = '../../CSVParser/daily_trading_details/dest_reduce/'

read_year = 0

Result = {  'start_date':[], 'test_date':[], \
            'start_week':[], 'end_week':[], 'test_week':[], \
            'train_period':[], 'test_period':[],'best_profit':[], 'best_win_profit':[], 'best_loss_profit':[], \
            'best_profit_factor':[], 'test_profit':[], 'test_win_profit':[], 'test_loss_profit':[], 'test_profit_factor':[], \
            'len_param':[], 'profit_factor_thres':[],\
            'buy_trade_count':[], 'buy_win_count':[], 'buy_win_ratio':[], 'buy_real_profit':[], 'buy_win_profit':[], 'buy_loss_profit':[], \
            'sell_trade_count':[], 'sell_win_count':[], 'sell_win_ratio':[], 'sell_real_profit':[], 'sell_win_profit':[], 'sell_loss_profit':[]};


def record_result(obj, strategist_obj):
    start_week, end_week, test_week, \
        train_period, test_period, best_profit, best_win_profit, best_loss_profit, best_profit_factor, \
        test_profit, test_win_profit, test_loss_profit, test_profit_factor, len_param, profit_factor_thres = obj.get_info()

    start_date = back_to_date(start_week)
    test_date = back_to_date(test_week)

    Result['start_date'].append(start_date.strftime('%Y%m%d'))
    Result['test_date'].append(test_date.strftime('%Y%m%d'))

    Result['start_week'].append(start_week)
    Result['end_week'].append(end_week)
    Result['test_week'].append(test_week)

    Result['train_period'].append(train_period)
    Result['test_period'].append(test_period)
    Result['best_profit'].append(best_profit)
    Result['best_win_profit'].append(best_win_profit)
    Result['best_loss_profit'].append(best_loss_profit)
    Result['best_profit_factor'].append(best_profit_factor)

    Result['test_profit'].append(test_profit)
    Result['test_win_profit'].append(test_win_profit)
    Result['test_loss_profit'].append(test_loss_profit)
    Result['test_profit_factor'].append(test_profit_factor)

    Result['len_param'].append(len_param)
    Result['profit_factor_thres'].append(profit_factor_thres)

    strategist_obj.settlement()

    buy_total_profit, buy_trade_count, buy_win_count, buy_win_ratio, buy_real_profit, buy_win_profit, buy_loss_profit, \
            sell_total_profit, sell_trade_count, sell_win_count, sell_win_ratio, sell_real_profit, sell_win_profit, sell_loss_profit = strategist_obj.get_info()

    Result['buy_trade_count'].append(buy_trade_count)
    Result['buy_win_count'].append(buy_win_count)
    Result['buy_win_ratio'].append(buy_win_ratio)
    Result['buy_real_profit'].append(buy_real_profit)
    Result['buy_win_profit'].append(buy_win_profit)
    Result['buy_loss_profit'].append(buy_loss_profit)

    Result['sell_trade_count'].append(sell_trade_count)
    Result['sell_win_count'].append(sell_win_count)
    Result['sell_win_ratio'].append(sell_win_ratio)
    Result['sell_real_profit'].append(sell_real_profit)
    Result['sell_win_profit'].append(sell_win_profit)
    Result['sell_loss_profit'].append(sell_loss_profit)





strategist_list = []
strategist_list_serial = []
static_min_handler = Static_Min.Static_Min(strategist_list, None, 0)

last_week = -1

while date_loop < End_Date:

    filename = datetime.strftime(date_loop, '%Y%m%d') + '_TX.csv'
    if not os.path.isfile(_file_path + filename):
        date_loop += delta
        continue

    print(date_loop)

    weeks = count_week(date_loop)
    
    if (-1 == last_week):
        last_week = weeks

    if (weeks > last_week):
        for train_weeks in range(24, 65, 12):
        # for train_weeks in range(2, 24, 2):
        # for train_weeks in range(24, 25, 24):
            # for test_weeks in range(1, 5, 1):
            for profit_factor_thres in range(2, 11, 1):
                profit_counter.append(Profit_Counter(train_weeks, 1, profit_counter_serial, profit_factor_thres))
                profit_counter_serial += 1

            # profit_counter.append(Profit_Counter(train_weeks, 1, profit_counter_serial, 10))
            # profit_counter_serial += 1

        last_week = weeks

    clear_idx = []
    clear_strategist_idx = []
    idx = 0

    df = pd.read_csv('static_min_trader/split/days/' + datetime.strftime(date_loop, '%Y%m%d') + '_split_file.csv', low_memory=False, memory_map=True, encoding="utf-8-sig")
    # print (df)

    temp_df = df.reset_index()
    for obj in profit_counter:
        obj.record_profit(weeks, temp_df)
        state = obj.get_status()

        if (DONE == state):
            clear_idx.append(idx)

            serial = obj.get_counter_serial()

            obj_idx = 0
            found = 0
            for search_serial in strategist_list_serial:
                if (search_serial == serial):
                    print ('found:', obj_idx)
                    found = 1
                    record_result(obj, strategist_list[obj_idx])
                    clear_strategist_idx.append(obj_idx)
                    break
                obj_idx += 1

            if (0 == found):
                print ('shit not found')

        elif (TESTING == state):
            train_period, get_param, best_param_list, counter_serial = obj.get_train_period()
            if (0 == get_param):
                add_strategist = Strategist_Min_Both_Multi.Strategist()
                for strategist_idx in range(0, len(best_param_list), 1):
                    add_strategist.add_strategist(best_param_list[strategist_idx][0], best_param_list[strategist_idx][1], \
                    best_param_list[strategist_idx][2], best_param_list[strategist_idx][3], best_param_list[strategist_idx][4], \
                    best_param_list[strategist_idx][5], best_param_list[strategist_idx][6], best_param_list[strategist_idx][7], \
                    best_param_list[strategist_idx][8])

                strategist_list.append(add_strategist)
                strategist_list_serial.append(counter_serial)

        idx += 1

    idx = 0
    for i in clear_idx:
        idx += 1
        profit_counter.pop(clear_idx[-idx])

    idx = 0
    for i in clear_strategist_idx:
        idx += 1
        strategist_list.pop(clear_strategist_idx[-idx])
        strategist_list_serial.pop(clear_strategist_idx[-idx])


    if (0 != len(strategist_list)):
        daily_trade(date_loop, filename, strategist_list, static_min_handler)

    date_loop += delta



dfLine = pd.DataFrame(Result)
dfLine.to_csv('static_min_trader/all_static_param_keep_both_low_train.csv', index=False, encoding='utf_8_sig');

