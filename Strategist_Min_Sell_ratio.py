import random
import sys
import os
import pandas as pd
import numpy as np
import math
from datetime import datetime, timedelta, tzinfo, timedelta

NONE = 4
SELL_ASKING = 2
SELL_ASKING = 5
SELL_ON_INTEREST = 3
SELL_ON_INTEREST = 6

WIN = 1
LOSS = 0

class Strategist:
    def __init__(self, stop_loss_ratio, stop_profit_ratio, move_loss, do_stop_profit, sell_high_new_thres, sell_low_new_thres, sell_close_new_thres, kd_thres, bolling_rz, \
                    KLine_low_rz, KLine_body_rz, KLine_upper_rz, KLine_trade, \
                    tower_thres, kline_step_ratio):
        
        self.stop_loss_ratio = stop_loss_ratio
        self.stop_profit_ratio = stop_profit_ratio
        self.kline_step_ratio = kline_step_ratio

        self.stop_loss = self.stop_profit = 0
        self.kline_step = 0

        self.move_loss = move_loss
        self.do_stop_profit = do_stop_profit
        self.sell_high_new_thres = sell_high_new_thres
        self.sell_low_new_thres = sell_low_new_thres
        self.sell_close_new_thres = sell_close_new_thres

        self.sell_close_new_thres_upper = sell_close_new_thres + 10
        self.kd_thres = kd_thres
        self.kd_thres_upper = kd_thres + 10

        self.total_profit = 0
        self.win_ratio = 0
        self.win_count = 0
        self.trade_count = 0
        self.real_profit = 0

        self.win_total = 0
        self.loss_total = 0

        self.highest = 0
        self.lowest = 0
        self.static_close = 0

        self.last_highest = 0
        self.last_lowest = 0
        self.last_static_close = 0

        self.status = NONE
        self.new_price = 0
        self.stop_loss_price = 0

        self.tick_profit = 0
        self.strategist_name = 'SELL'

        self.H_line = self.M_line = self.L_line = 0
        self.bolling_rz = bolling_rz

        self.KLine_upper_rz = KLine_upper_rz
        self.KLine_body_rz = KLine_body_rz
        self.KLine_low_rz = KLine_low_rz
        self.KLine_trade = KLine_trade

        self.tower_thres = tower_thres


    def close_deal(self, close):

        if (SELL_ON_INTEREST == self.status):
            self.tick_profit = self.new_price - close
            if (self.tick_profit > 0):
                self.win_count += 1
                self.win_total += self.tick_profit - 1
            else:
                self.loss_total += self.tick_profit - 1

            self.total_profit += self.tick_profit
            self.status = NONE


    def handle_with_tick(self, ts, close):

        if (0 == self.stop_loss):
            self.stop_loss = close * self.stop_loss_ratio / 1000
            self.stop_profit = close * self.stop_profit_ratio / 1000
            self.kline_step = close * self.kline_step_ratio / 1000

        if (SELL_ASKING == self.status):
            self.new_price = close
            self.stop_loss_price = close + self.stop_loss
            self.trade_count += 1

            print ('sell new:', close, ', stop_loss:', self.stop_loss_price)
            self.status = SELL_ON_INTEREST

        elif (SELL_ON_INTEREST == self.status):

            if (close > self.stop_loss_price):
                self.close_deal(close)
                print ('stop loss:', close, ', profit:', self.tick_profit, ', total_profit:', self.total_profit)

            elif (close < self.new_price - self.stop_profit and 1 == self.do_stop_profit):
                self.close_deal(close)
                print ('stop profit:', close, ', profit:', self.tick_profit, ', total_profit:', self.total_profit)

    def handle_with_static_ema_bolling(self, close, high, low, slowk, slowd, ema_fast, ema_middle, ema_slow, H_line, M_line, L_line):

        self.highest = high
        self.lowest = low
        self.static_close = close

        if (NONE == self.status and 0 != self.last_highest and False == math.isnan(slowk)):
            # if (self.highest - self.last_highest > self.sell_high_new_thres and self.lowest - self.last_lowest > self.sell_low_new_thres and \
            #     self.static_close - self.last_static_close > self.sell_close_new_thres):

            if (self.last_static_close - self.static_close > self.sell_close_new_thres and \
                slowk > self.kd_thres and slowk < self.kd_thres_upper and \
                slowd > self.kd_thres and slowd < self.kd_thres_upper):
                # self.status = SELL_ASKING

                if (self.static_close < ema_fast and ema_fast < ema_middle and ema_middle < ema_slow):
                    # self.status = SELL_ASKING

                    if ((self.bolling_rz == 0 and close < L_line) or \
                        (self.bolling_rz == 1 and close >= L_line and close < M_line) or \
                        (self.bolling_rz == 2 and close >= M_line and close < H_line) or \
                        (self.bolling_rz == 3 and close >= H_line)):

                        self.status = SELL_ASKING

                # if (self.static_close < ema_fast and ema_fast < ema_middle):
                #     self.status = SELL_ASKING

            # if (self.last_static_close - self.static_close > self.sell_close_new_thres and \
            #     self.last_static_close - self.static_close < self.sell_close_new_thres_upper and \
            #     slowk > self.kd_thres and slowk < self.kd_thres_upper and \
            #     slowd > self.kd_thres and slowd < self.kd_thres_upper):
            #     self.status = SELL_ASKING

        elif (1 == self.move_loss and SELL_ON_INTEREST == self.status):
            if (low + self.stop_loss < self.stop_loss_price):
                self.stop_loss_price = low + self.stop_loss

        self.last_highest = self.highest
        self.last_lowest = self.lowest
        self.last_static_close = self.static_close

    def handle_with_static_ema(self, close, high, low, slowk, slowd, ema_fast, ema_middle, ema_slow, H_line, M_line, L_line):

        self.highest = high
        self.lowest = low
        self.static_close = close

        if (NONE == self.status and 0 != self.last_highest and False == math.isnan(slowk)):
            # if (self.highest - self.last_highest > self.sell_high_new_thres and self.lowest - self.last_lowest > self.sell_low_new_thres and \
            #     self.static_close - self.last_static_close > self.sell_close_new_thres):

            if (self.last_static_close - self.static_close > self.sell_close_new_thres and \
                slowk > self.kd_thres and slowk < self.kd_thres_upper and \
                slowd > self.kd_thres and slowd < self.kd_thres_upper):
                # self.status = SELL_ASKING

                if (self.static_close < ema_fast and ema_fast < ema_middle and ema_middle < ema_slow):
                    self.status = SELL_ASKING

                # if (self.static_close < ema_fast and ema_fast < ema_middle):
                #     self.status = SELL_ASKING

            # if (self.last_static_close - self.static_close > self.sell_close_new_thres and \
            #     self.last_static_close - self.static_close < self.sell_close_new_thres_upper and \
            #     slowk > self.kd_thres and slowk < self.kd_thres_upper and \
            #     slowd > self.kd_thres and slowd < self.kd_thres_upper):
            #     self.status = SELL_ASKING

        elif (1 == self.move_loss and SELL_ON_INTEREST == self.status):
            if (low + self.stop_loss < self.stop_loss_price):
                self.stop_loss_price = low + self.stop_loss

        self.last_highest = self.highest
        self.last_lowest = self.lowest
        self.last_static_close = self.static_close
            

    def handle_with_tick_bolling(self, close):

        if (NONE == self.status and self.H_line != 0):
            if (self.L_line - close > self.sell_close_new_thres):
                self.status = SELL_ASKING

        elif (SELL_ASKING == self.status):
            self.new_price = close
            self.stop_loss_price = close + self.stop_loss
            self.trade_count += 1

            print ('sell new:', close, ', stop_loss:', self.stop_loss_price)
            self.status = SELL_ON_INTEREST

        elif (SELL_ON_INTEREST == self.status):

            if (close > self.stop_loss_price):
                self.close_deal(close)
                print ('stop loss:', close, ', profit:', self.tick_profit, ', total_profit:', self.total_profit)

            elif (close < self.new_price - self.stop_profit and 1 == self.do_stop_profit):
                self.close_deal(close)
                print ('stop profit:', close, ', profit:', self.tick_profit, ', total_profit:', self.total_profit)

    def handle_with_static_bolling(self, H_line, M_line, L_line):
        if (False == math.isnan(H_line)):
            self.H_line = H_line
            self.M_line = M_line
            self.L_line = L_line


    def handle_with_static_tower(self, close, high_array, low_array):
        if (NONE == self.status and len(high_array) > self.tower_thres):
            if (close < min(low_array[-(self.tower_thres + 1):-1])):
                print ('tower selling')
                self.status = SELL_ASKING

        elif (SELL_ON_INTEREST == self.status):
            if (close < max(high_array[-(self.tower_thres + 1):-1])):
                print ('tower closing')
                self.stop_loss_price = -1

    def price_to_rz(self, price):
        start_idx = self.kline_step
        idx_jump = self.kline_step

        for idx in range(0, 5, 1):
            if (price < idx * idx_jump + start_idx):
                return idx

        return 5

    def handle_with_static_KLine_bolling(self, close, high, low, slowk, slowd, ema_fast, ema_middle, ema_slow, H_line, M_line, L_line):

        self.highest = high
        self.lowest = low
        self.static_close = close

        # if (NONE == self.status and 0 != self.last_highest and False == math.isnan(slowk)):
            # if (self.highest - self.last_highest > self.sell_high_new_thres and self.lowest - self.last_lowest > self.sell_low_new_thres and \
            #     self.static_close - self.last_static_close > self.sell_close_new_thres):

        if (NONE == self.status and self.last_static_close != 0):

            if (self.static_close >= self.last_static_close):
                low_line = self.last_static_close - self.lowest
                body_line = self.static_close - self.last_static_close
                upper_line = self.highest - self.static_close
                trade = 1
            else:
                low_line = self.static_close - self.lowest
                body_line = self.last_static_close - self.static_close
                upper_line = self.highest - self.last_static_close
                trade = 0

            if (trade == self.KLine_trade and self.KLine_low_rz == self.price_to_rz(low_line) and self.KLine_body_rz == self.price_to_rz(body_line) and \
                self.KLine_upper_rz == self.price_to_rz(upper_line)):

                if ((self.bolling_rz == 0 and close < L_line) or \
                    (self.bolling_rz == 1 and close >= L_line and close < M_line) or \
                    (self.bolling_rz == 2 and close >= M_line and close < H_line) or \
                    (self.bolling_rz == 3 and close >= H_line)):
                
                    self.status = SELL_ASKING


        elif (1 == self.move_loss and SELL_ON_INTEREST == self.status):
            if (low + self.stop_loss < self.stop_loss_price):
                self.stop_loss_price = low + self.stop_loss

        self.last_highest = self.highest
        self.last_lowest = self.lowest
        self.last_static_close = self.static_close

    def handle_with_static_KLine(self, close, high, low, slowk, slowd, ema_fast, ema_middle, ema_slow, H_line, M_line, L_line):

        if (0 == self.stop_loss):
            self.stop_loss = close * self.stop_loss_ratio / 1000
            self.stop_profit = close * self.stop_profit_ratio / 1000
            self.kline_step = close * self.kline_step_ratio / 1000

        self.highest = high
        self.lowest = low
        self.static_close = close

        # if (NONE == self.status and 0 != self.last_highest and False == math.isnan(slowk)):
            # if (self.highest - self.last_highest > self.sell_high_new_thres and self.lowest - self.last_lowest > self.sell_low_new_thres and \
            #     self.static_close - self.last_static_close > self.sell_close_new_thres):

        if (NONE == self.status and self.last_static_close != 0):

            if (self.static_close >= self.last_static_close):
                low_line = self.last_static_close - self.lowest
                body_line = self.static_close - self.last_static_close
                upper_line = self.highest - self.static_close
                trade = 1
            else:
                low_line = self.static_close - self.lowest
                body_line = self.last_static_close - self.static_close
                upper_line = self.highest - self.last_static_close
                trade = 0

            if (trade == self.KLine_trade and self.KLine_low_rz == self.price_to_rz(low_line) and self.KLine_body_rz == self.price_to_rz(body_line) and \
                self.KLine_upper_rz == self.price_to_rz(upper_line)):
                self.status = SELL_ASKING


        elif (1 == self.move_loss and SELL_ON_INTEREST == self.status):
            if (low + self.stop_loss < self.stop_loss_price):
                self.stop_loss_price = low + self.stop_loss

        self.last_highest = self.highest
        self.last_lowest = self.lowest
        self.last_static_close = self.static_close

    def handle_with_static(self, close, high, low, slowk, slowd):

        self.highest = high
        self.lowest = low
        self.static_close = close

        if (NONE == self.status and 0 != self.last_highest and False == math.isnan(slowk)):
            # if (self.highest - self.last_highest > self.sell_high_new_thres and self.lowest - self.last_lowest > self.sell_low_new_thres and \
            #     self.static_close - self.last_static_close > self.sell_close_new_thres):
            # if (self.last_static_close - self.static_close > self.sell_close_new_thres and \
            #     slowk > self.kd_thres and slowk < self.kd_thres_upper and \
            #     slowd > self.kd_thres and slowd < self.kd_thres_upper):
            #     self.status = SELL_ASKING

            if (self.last_static_close - self.static_close > self.sell_close_new_thres and \
                self.last_static_close - self.static_close < self.sell_close_new_thres_upper and \
                slowk > self.kd_thres and slowk < self.kd_thres_upper and \
                slowd > self.kd_thres and slowd < self.kd_thres_upper):
                self.status = SELL_ASKING

        elif (1 == self.move_loss and SELL_ON_INTEREST == self.status):
            if (low + self.stop_loss < self.stop_loss_price):
                self.stop_loss_price = low + self.stop_loss

        self.last_highest = self.highest
        self.last_lowest = self.lowest
        self.last_static_close = self.static_close
            
    def force_close(self, tick_price):

        if (SELL_ON_INTEREST == self.status):
            self.close_deal(tick_price)
            print ('force close:', tick_price, ', profit:', self.tick_profit, ', total_profit:', self.total_profit)

        self.stop_loss = self.stop_profit = 0
        self.kline_step = 0

        
    def settlement(self):
        if (self.trade_count != 0):
            self.win_ratio = self.win_count/self.trade_count
        self.real_profit = self.total_profit - self.trade_count

    def report(self):
        attrs = vars(self)
        print(', '.join("%s: %s" % item for item in attrs.items()))

    def get_info(self):
        return self.total_profit, self.trade_count, self.win_count, self.win_ratio, self.real_profit, self.win_total, self.loss_total, \
            self.stop_loss_ratio, self.stop_profit_ratio, self.move_loss, self.do_stop_profit, self.sell_high_new_thres, self.sell_low_new_thres, self.sell_close_new_thres, self.kd_thres, self.bolling_rz, \
            self.KLine_low_rz, self.KLine_body_rz, self.KLine_upper_rz, self.KLine_trade, self.tower_thres, self.kline_step_ratio

    def refresh_tick_profit(self):
        if (sys.maxsize != self.tick_profit):
            profit = self.tick_profit - 1
        else:
            profit = sys.maxsize
        
        self.tick_profit = sys.maxsize

        return profit
        
    def clear_states(self):
        self.status = NONE

    def get_status(self):
        return self.status

    def get_strategist_name(self):
        return self.strategist_name
