import pandas as pd
import sys
import os
import numpy as np
import math
from datetime import datetime, timedelta, tzinfo, timedelta
from dateutil.relativedelta import relativedelta
import talib

INTERVAL_TIME = 15*60

NONE = 4
BUY_ASKING = 2
SELL_ASKING = 5
BUY_ON_INTEREST = 3
SELL_ON_INTEREST = 6

class Static_Min:
    def __init__(self, strategist_list, result, record):

        self.highArray = []
        self.lowArray = []
        self.closeArray = []

        self.nightStart = datetime.strptime("150000", "%H%M%S")
        self.midNightStart = datetime.strptime("000000", "%H%M%S")
        self.nightEnd = datetime.strptime("50000", "%H%M%S")

        self.dayStart = datetime.strptime("84500", "%H%M%S")
        self.endTime = datetime.strptime("134500", "%H%M%S")

        self.tradeStart = datetime.strptime("84500", "%H%M%S")
        self.tradeEnd = datetime.strptime("133000", "%H%M%S")

        self.strategist_list = strategist_list
        self.Result = result
        self.record = record

        self.checkNight = 0
        self.lastClose = 0
        self.highest = 0
        self.lowest = 999999
        self.timeCheck = datetime.strptime("84500", "%H%M%S") + timedelta(seconds = INTERVAL_TIME)
        # self.timeCheck = datetime.strptime("84500", "%H%M%S")

        self.k_save = self.d_save = 0
        self.H_line_save = self.M_line_save = self.L_line_save = 0
        self.new_time_save = datetime.strptime("84500", "%H%M%S")
        self.buy_sell_save = 0

    def clear_daily(self):

        self.checkNight = 0
        self.lastClose = 0
        self.highest = 0
        self.lowest = 999999
        self.timeCheck = datetime.strptime("84500", "%H%M%S") + timedelta(seconds = INTERVAL_TIME)
        # self.timeCheck = datetime.strptime("84500", "%H%M%S")

    def assigned_result(self, date, profit, time):
        self.Result['date'].append(date)
        self.Result['profit'].append(profit)
        self.Result['close_time'].append(time)
        self.Result['new_time'].append(self.new_time_save.strftime("%H%M%S"))
        self.Result['buy_sell'].append(self.buy_sell_save)

    def static_tick(self, date, ts, tick):

        if (self.nightStart.hour == ts.hour and self.nightStart.minute == ts.minute and
                self.checkNight == 0):

            self.timeCheck = self.nightStart + timedelta(seconds = INTERVAL_TIME)
            # self.timeCheck = self.nightStart
            self.checkNight = 1

        elif (0 == ts.hour and self.checkNight == 1):
            self.timeCheck = self.midNightStart
            self.checkNight = 2

        elif (self.dayStart.hour == ts.hour and self.dayStart.minute == ts.minute and self.checkNight == 2):

            self.timeCheck = self.nightEnd
            self.static_min(self.nightEnd, self.lastClose)

            for obj in self.strategist_list:
                obj.force_close(self.lastClose)
                
                profit = obj.refresh_tick_profit()
                if (1 == self.record):
                    if (profit != sys.maxsize):
                        self.assigned_result(date.strftime("%Y%m%d"), profit, self.nightEnd.strftime("%H%M%S"))

            self.timeCheck = datetime.strptime("84500", "%H%M%S") + timedelta(seconds = INTERVAL_TIME)
            # self.timeCheck = datetime.strptime("84500", "%H%M%S")

            self.checkNight = 3

            # profit = assign_worker(list, REFRESH_TICK_PROFIT, 0, 0, 0, 0, 0)
            # if (profit != sys.maxsize):
            #     assigned_result(date.strftime("%Y%m%d"), profit, nightEnd.strftime("%H%M%S"), k_save, d_save, new_time_save, buy_sell_save)

        # self.static_min(ts, self.lastClose)

        # if ('20170803' not in filename):
        for obj in self.strategist_list:
            obj.handle_with_tick(ts, tick)

            profit = obj.refresh_tick_profit()
            if (1 == self.record):
                if (profit != sys.maxsize):
                    self.assigned_result(date.strftime("%Y%m%d"), profit, ts.strftime("%H%M%S"))

                status = obj.get_status()
                if (BUY_ASKING == status or SELL_ASKING == status):
                    self.new_time_save = ts
                    if (BUY_ASKING == status):
                        self.buy_sell_save = 1
                    if (SELL_ASKING == status):
                        self.buy_sell_save = 2

        self.lastClose = tick

        if (tick > self.highest):
            self.highest = tick

        if (tick < self.lowest):
            self.lowest = tick

        self.static_min(ts, tick)

    def static_min(self, ts, close):

        # print ('in:', self.timeCheck.time(), ts.time())

        if (self.timeCheck <= ts):

            self.highArray.append(self.highest)
            self.lowArray.append(self.lowest)
            self.closeArray.append(close)

            if (len(self.highArray) >= 100):
                self.highArray.pop(0)
                self.lowArray.pop(0)
                self.closeArray.pop(0)

            high_series = np.asarray(self.highArray)
            low_series = np.asarray(self.lowArray)
            close_series = np.asarray(self.closeArray)

            slowk, slowd = talib.STOCH(high_series, low_series, close_series, fastk_period=9, slowk_period=3,
                                       slowk_matype=0, slowd_period=3,
                                       slowd_matype=0)  

            ema_fast = talib.EMA(close_series, timeperiod=5)
            ema_middle = talib.EMA(close_series, timeperiod=10)
            ema_slow = talib.EMA(close_series, timeperiod=20)

            H_line, M_line, L_line = talib.BBANDS(close_series, timeperiod=5, nbdevup=2, nbdevdn=2, matype=0)

            for obj in self.strategist_list:
                # obj.handle_with_static_ema_bolling(close, self.highest, self.lowest, slowk[-1], slowd[-1], ema_fast[-1], \
                #                                     ema_middle[-1], ema_slow[-1], H_line[-1], M_line[-1], L_line[-1])

                # obj.handle_with_static_ema(close, self.highest, self.lowest, slowk[-1], slowd[-1], ema_fast[-1], \
                #                                     ema_middle[-1], ema_slow[-1], H_line[-1], M_line[-1], L_line[-1])

                obj.handle_with_static_KLine(close, self.highest, self.lowest, slowk[-1], slowd[-1], ema_fast[-1], \
                                                    ema_middle[-1], ema_slow[-1], H_line[-1], M_line[-1], L_line[-1])
            
                # obj.handle_with_static_bolling(close, self.highest, self.lowest, slowk[-1], slowd[-1], ema_fast[-1], \
                #                                     ema_middle[-1], ema_slow[-1], H_line[-1], M_line[-1], L_line[-1])

                status = obj.get_status()
                if (BUY_ASKING == status or SELL_ASKING == status):
                    self.new_time_save = ts
                    if (BUY_ASKING == status):
                        self.buy_sell_save = 1
                    if (SELL_ASKING == status):
                        self.buy_sell_save = 2

            print (self.timeCheck.time(), ts.time())
            # print (close, self.highest, self.lowest)

            # if (timeCheck >= endTime):
            #     break
            # 幾分鐘線
            while (self.timeCheck.time() <= ts.time()):
                self.timeCheck += timedelta(seconds = INTERVAL_TIME)
                if (0 != ts.hour and 0 == self.timeCheck.hour):
                    break

            if (self.timeCheck.time() == self.nightEnd.time()):
                while (self.timeCheck.time() < self.tradeStart.time()):
                    self.timeCheck += timedelta(seconds = INTERVAL_TIME)

            self.highest = 0
            self.lowest = 999999
