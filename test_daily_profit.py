import pandas as pd
import sys
import os
import numpy as np
import math
from datetime import datetime, timedelta, tzinfo, timedelta
from dateutil.relativedelta import relativedelta
import Strategist_Min
import Strategist_Min_Sell
# import Strategist_Min_Sep
# import Strategist_Min_Both
# import Strategist_Multi
import talib
import Static_Min

DATE = '成交日期'
CONTRACT = '商品代號'
EXPIRED_MONTH = '到期月份(週別)'
TIME = '成交時間'
PRICE = '成交價格'
VOLUME = '成交數量(B+S)'

_file_path = '../../CSVParser/daily_trading_details/dest_reduce/'
# _file_path = '../daily_trading_details/dest_reduce/'

HANDLE_WITH_STATIC = 1
FORCE_CLOSE = 2
SETTLEMENT = 3
REPORT = 4
CLEAR_TRADE_INFO = 5
HANDLE_WITH_TICK = 6
HANDLE_WITH_TICK_REF_STATIC = 7
HANDLE_WITH_MOVE_TICK = 8
HANDLE_WITH_STATIC_ALL_TICK = 9
STATIC_FORCE_CLOSE = 10
HANDLE_WITH_ALL_TICK_SHIFT = 11
HANDLE_WITH_ALL_TICK_SHIFT_HIGH = 12
HANDLE_WITH_ALL_TICK_ONE_SHIFT_HIGH = 13
HANDLE_WITH_ALL_TICK_ONE_SHIFT_HIGH_LOW_LIMIT = 14
REFRESH_TICK_PROFIT = 15
CLEAR_STATE = 16
GET_STATE = 17
HANDLE_WITH_TICK_BOLLING = 18



def assign_worker(list, behavior, close, high, low, slowk, slowd):
    for obj in list:
        if (HANDLE_WITH_STATIC == behavior):
            obj.handle_with_static(close, high, low, slowk, slowd)
        elif (FORCE_CLOSE == behavior):
            obj.force_close(close)
        elif (SETTLEMENT == behavior):
            obj.settlement()
        elif (REPORT == behavior):
            obj.report()
        elif (CLEAR_TRADE_INFO == behavior):
            obj.clear_trade_info()
        elif (HANDLE_WITH_TICK == behavior):
            obj.handle_with_tick(close)

        elif (REFRESH_TICK_PROFIT == behavior):
            return obj.refresh_tick_profit()

        elif (CLEAR_STATE == behavior):
            obj.clear_states()

        elif (GET_STATE == behavior):
            return obj.get_status()

        elif (HANDLE_WITH_TICK_BOLLING == behavior):
            obj.handle_with_tick_bolling(close)


def assigned_result(date, profit, time, obj):

    total_profit, trade_count, win_count, win_ratio, real_profit, win_total, loss_total, stop_loss, stop_profit, \
        move_loss, do_stop_profit, buy_high_new_thres, buy_low_new_thres, buy_close_new_thres, kd_thres, bolling_rz, \
        KLine_low_rz, KLine_body_rz, KLine_upper_rz, KLine_trade, tower_thres, kline_step_ratio, buy_sell = obj.get_info()

    Result['date'].append(date)
    Result['profit'].append(profit)
    Result['stop_loss'].append(stop_loss)
    Result['stop_profit'].append(stop_profit)
    Result['move_loss'].append(move_loss)
    Result['do_stop_profit'].append(do_stop_profit)
    
    Result['KLine_low_rz'].append(KLine_low_rz)
    Result['KLine_body_rz'].append(KLine_body_rz)
    Result['KLine_upper_rz'].append(KLine_upper_rz)
    Result['KLine_trade'].append(KLine_trade)

    Result['buy_sell'].append(buy_sell)    


def daily_trade(date, filename, list, static_min_handler, Result, save_file_path):

    _daily_df = pd.read_csv(_file_path + filename, low_memory=False, memory_map=True)
    print (filename)

    k_save = d_save = 0
    new_time_save = datetime.strptime("84500", "%H%M%S")
    buy_sell_save = 0

    highest = 0
    lowest = 999999
    found = ema_count = 0

    closePrice = 0
    checkNight = 0

    endTime = datetime.strptime("134500", "%H%M%S")

    # timeCheck = dayStart

    for index, row in _daily_df.iterrows():

        ts = datetime.strptime(str(row[TIME]).zfill(6), "%H%M%S")

        static_min_handler.static_tick(date, ts, row[PRICE])

        lastClose = row[PRICE]

    static_min_handler.static_min(endTime, lastClose)

    assign_worker(list, FORCE_CLOSE, lastClose, 0, 0, 0, 0)

    for obj in list:
        profit = obj.refresh_tick_profit()
        daily_profit = obj.get_daily_profit()

        assigned_result(date.strftime("%Y%m%d"), daily_profit, ts.strftime("%H%M%S"), obj)

    dfLine = pd.DataFrame(Result)
    dfLine.to_csv(save_file_path, mode='a', index=False, encoding='utf_8_sig', header=False);

    static_min_handler.clear_daily()



strategist_list = []

# for KLine_ratio
# for kline_step_ratio in range(1, 5, 1):
#     for stop_loss_ratio in range(1, 10, 2):
#         for KLine_low_rz in range(0, 6, 1):
#             for KLine_body_rz in range(0, 6, 1):
#                 for KLine_upper_rz in range(0, 6, 1):
#                     for KLine_trade in range(0, 2, 1):
#                         for do_stop_profit in range (0, 2, 1):
#                             if (1 == do_stop_profit):
#                                 for move_loss in range(0, 2, 1):
#                                     for stop_profit_ratio in range(1, 20, 2):
#                                         strategist_list.append(Strategist_Min_ratio.Strategist(stop_loss_ratio, stop_profit_ratio, move_loss, do_stop_profit, 0, 0, 0, 0, 0, \
#                                             KLine_low_rz, KLine_body_rz, KLine_upper_rz, KLine_trade, 0, kline_step_ratio))
#                             else:
#                                 strategist_list.append(Strategist_Min_ratio.Strategist(stop_loss_ratio, 0, 1, do_stop_profit, 0, 0, 0, 0, 0, \
#                                             KLine_low_rz, KLine_body_rz, KLine_upper_rz, KLine_trade, 0, kline_step_ratio))

# for kline_step_ratio in range(1, 5, 1):
#     for stop_loss_ratio in range(1, 10, 2):
#         for KLine_low_rz in range(0, 6, 1):
#             for KLine_body_rz in range(0, 6, 1):
#                 for KLine_upper_rz in range(0, 6, 1):
#                     for KLine_trade in range(0, 2, 1):
#                         for do_stop_profit in range (0, 2, 1):
#                             if (1 == do_stop_profit):
#                                 for move_loss in range(0, 2, 1):
#                                     for stop_profit_ratio in range(1, 20, 2):
#                                         strategist_list.append(Strategist_Min_Sell_ratio.Strategist(stop_loss_ratio, stop_profit_ratio, move_loss, do_stop_profit, 0, 0, 0, 0, 0, \
#                                             KLine_low_rz, KLine_body_rz, KLine_upper_rz, KLine_trade, 0, kline_step_ratio))
#                             else:
#                                 strategist_list.append(Strategist_Min_Sell_ratio.Strategist(stop_loss_ratio, 0, 1, do_stop_profit, 0, 0, 0, 0, 0, \
#                                             KLine_low_rz, KLine_body_rz, KLine_upper_rz, KLine_trade, 0, kline_step_ratio))

# Kline
for bolling_rz in range(0, 4, 1):
    for stop_loss in range(20, 220, 30):
        for KLine_low_rz in range(0, 6, 1):
            for KLine_body_rz in range(0, 6, 1):
                for KLine_upper_rz in range(0, 6, 1):
        # for KLine_low_rz in range(0, 11, 1):
        #     for KLine_body_rz in range(0, 11, 1):
        #         for KLine_upper_rz in range(0, 11, 1):
                    for KLine_trade in range(0, 2, 1):
                        for do_stop_profit in range (0, 2, 1):
                            if (1 == do_stop_profit):
                                for move_loss in range(0, 2, 1):
                                    for stop_profit in range(50, 320, 50):
                                        strategist_list.append(Strategist_Min.Strategist(stop_loss, stop_profit, move_loss, do_stop_profit, 0, 0, 0, 0, bolling_rz, \
                                            KLine_low_rz, KLine_body_rz, KLine_upper_rz, KLine_trade, 0))
                            # else:
                            #     strategist_list.append(Strategist_Min.Strategist(stop_loss, 0, 1, do_stop_profit, 0, 0, 0, 0, 0, \
                            #                 KLine_low_rz, KLine_body_rz, KLine_upper_rz, KLine_trade, 0))

for bolling_rz in range(0, 4, 1):
    for stop_loss in range(20, 220, 30):
        for KLine_low_rz in range(0, 6, 1):
            for KLine_body_rz in range(0, 6, 1):
                for KLine_upper_rz in range(0, 6, 1):
        # for KLine_low_rz in range(0, 11, 1):
        #     for KLine_body_rz in range(0, 11, 1):
        #         for KLine_upper_rz in range(0, 11, 1):
                    for KLine_trade in range(0, 2, 1):
                        for do_stop_profit in range (0, 2, 1):
                            if (1 == do_stop_profit):
                                for move_loss in range(0, 2, 1):
                                    for stop_profit in range(50, 320, 50):
                                        strategist_list.append(Strategist_Min_Sell.Strategist(stop_loss, stop_profit, move_loss, do_stop_profit, 0, 0, 0, 0, bolling_rz, \
                                            KLine_low_rz, KLine_body_rz, KLine_upper_rz, KLine_trade, 0))
                            # else:
                            #     strategist_list.append(Strategist_Min_Sell.Strategist(stop_loss, 0, 1, do_stop_profit, 0, 0, 0, 0, 0, \
                            #                 KLine_low_rz, KLine_body_rz, KLine_upper_rz, KLine_trade, 0))


# strategist_list.append(Strategist_Min.Strategist(180, 210, 1, 1, 0, 0, 0, 0, 0, \
#                                         0, 5, 1, 1, 0))

# strategist_list.append(Strategist_Min.Strategist(10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))
# strategist_list.append(Strategist_Min_Sell.Strategist(10, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0))

year = 2021

Start_Date = datetime(year, 1, 1, 0, 0)
End_Date = datetime(year + 1, 1, 1, 0, 0)

date_loop = Start_Date
delta = timedelta(days=1)

static_min_handler = Static_Min.Static_Min(strategist_list, None, 0)

save_file_path = 'static_min_trader/' + str(year) + '_close_static_15min_trader_kline_bolling.csv'

Result = {'date':[], 'profit':[], 'stop_loss':[], 'stop_profit':[], 'move_loss':[], 'do_stop_profit':[], \
            'KLine_low_rz':[], 'KLine_body_rz':[], 'KLine_upper_rz':[], 'KLine_trade':[], 'buy_sell':[]}

dfLine = pd.DataFrame(Result)
dfLine.to_csv(save_file_path, index=False, encoding='utf_8_sig');

while date_loop <= End_Date:
    # print(date_loop)

    filename = datetime.strftime(date_loop, '%Y%m%d') + '_TX.csv'
    if not os.path.isfile(_file_path + filename):
        date_loop += delta
        continue

    daily_trade(date_loop, filename, strategist_list, static_min_handler, Result, save_file_path)

    date_loop += delta

    Result = {'date':[], 'profit':[], 'stop_loss':[], 'stop_profit':[], 'move_loss':[], 'do_stop_profit':[], \
            'KLine_low_rz':[], 'KLine_body_rz':[], 'KLine_upper_rz':[], 'KLine_trade':[], 'buy_sell':[]}

    # break


Result_Total = {'stop_loss_ratio':[], 'stop_profit_ratio':[], 'move_loss':[], 'do_stop_profit':[], 'buy_high_new_thres':[], 'buy_low_new_thres':[], 'buy_close_new_thres':[], \
            'total_profit':[], 'trade_count':[], 'win_count':[], 'win_ratio':[], 'real_profit':[], 'win_total':[], 'loss_total':[], 'kd_thres':[], 'bolling_rz':[], \
            'KLine_low_rz':[], 'KLine_body_rz':[], 'KLine_upper_rz':[], 'KLine_trade':[], \
            'tower_thres':[], 'kline_step_ratio':[], 'buy_sell':[]}

for obj in strategist_list:
    
    obj.settlement()        

    total_profit, trade_count, win_count, win_ratio, real_profit, win_total, loss_total, stop_loss_ratio, stop_profit_ratio, \
        move_loss, do_stop_profit, buy_high_new_thres, buy_low_new_thres, buy_close_new_thres, kd_thres, bolling_rz, \
        KLine_low_rz, KLine_body_rz, KLine_upper_rz, KLine_trade, tower_thres, kline_step_ratio, buy_sell = obj.get_info()

    Result_Total['total_profit'].append(total_profit)
    Result_Total['trade_count'].append(trade_count)
    Result_Total['win_count'].append(win_count)
    Result_Total['win_ratio'].append(win_ratio)
    Result_Total['real_profit'].append(real_profit)
    Result_Total['win_total'].append(win_total)
    Result_Total['loss_total'].append(loss_total)
    Result_Total['stop_loss_ratio'].append(stop_loss_ratio)

    Result_Total['stop_profit_ratio'].append(stop_profit_ratio)
    Result_Total['move_loss'].append(move_loss)
    Result_Total['do_stop_profit'].append(do_stop_profit)
    Result_Total['buy_high_new_thres'].append(buy_high_new_thres)
    Result_Total['buy_low_new_thres'].append(buy_low_new_thres)
    Result_Total['buy_close_new_thres'].append(buy_close_new_thres)
    Result_Total['kd_thres'].append(kd_thres)
    Result_Total['bolling_rz'].append(bolling_rz)

    Result_Total['KLine_low_rz'].append(KLine_low_rz)
    Result_Total['KLine_body_rz'].append(KLine_body_rz)
    Result_Total['KLine_upper_rz'].append(KLine_upper_rz)
    Result_Total['KLine_trade'].append(KLine_trade)

    Result_Total['tower_thres'].append(tower_thres)

    Result_Total['kline_step_ratio'].append(kline_step_ratio)
    Result_Total['buy_sell'].append(buy_sell)


dfLine = pd.DataFrame(Result_Total)
dfLine.to_csv('static_min_trader/' + str(year) + '_all_static_15min_trader_kline_bolling.csv', index=False, encoding='utf_8_sig');

