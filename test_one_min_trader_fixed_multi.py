import pandas as pd
import sys
import os
import numpy as np
import math
parent_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.sys.path.insert(0, parent_dir)
from datetime import datetime, timedelta, tzinfo, timedelta
from dateutil.relativedelta import relativedelta
import Strategist_Min
import Strategist_Min_Sell
# import Strategist_Min_Sep
# import Strategist_Min_Both
# import Strategist_Multi
import Strategist_Min_Both_Multi
import talib
import Static_Min

DATE = '成交日期'
CONTRACT = '商品代號'
EXPIRED_MONTH = '到期月份(週別)'
TIME = '成交時間'
PRICE = '成交價格'
VOLUME = '成交數量(B+S)'

# _file_path = '../../CSVParser/daily_trading_details/dest_reduce/'
_file_path = '../daily_trading_details/dest_reduce/'

HANDLE_WITH_STATIC = 1
FORCE_CLOSE = 2
SETTLEMENT = 3
REPORT = 4
CLEAR_TRADE_INFO = 5
HANDLE_WITH_TICK = 6
HANDLE_WITH_TICK_REF_STATIC = 7
HANDLE_WITH_MOVE_TICK = 8
HANDLE_WITH_STATIC_ALL_TICK = 9
STATIC_FORCE_CLOSE = 10
HANDLE_WITH_ALL_TICK_SHIFT = 11
HANDLE_WITH_ALL_TICK_SHIFT_HIGH = 12
HANDLE_WITH_ALL_TICK_ONE_SHIFT_HIGH = 13
HANDLE_WITH_ALL_TICK_ONE_SHIFT_HIGH_LOW_LIMIT = 14
REFRESH_TICK_PROFIT = 15
CLEAR_STATE = 16
GET_STATE = 17
HANDLE_WITH_TICK_BOLLING = 18

NONE = 4
BUY_ASKING = 2
SELL_ASKING = 5
BUY_ON_INTEREST = 3
SELL_ON_INTEREST = 6


Result = {'date':[], 'profit':[], 'close_time':[], 'new_time':[], 'buy_sell':[]}

year = 2020
Start_Date = datetime(year, 1, 1, 0, 0)
End_Date = datetime(2021 , 1, 1, 0, 0)

highArray = []
lowArray = []
closeArray = []


def assign_worker(list, behavior, close, high, low, slowk, slowd):
    for obj in list:
        if (HANDLE_WITH_STATIC == behavior):
            obj.handle_with_static(close, high, low, slowk, slowd)
        elif (FORCE_CLOSE == behavior):
            obj.force_close(close)
        elif (SETTLEMENT == behavior):
            obj.settlement()
        elif (REPORT == behavior):
            obj.report()
        elif (CLEAR_TRADE_INFO == behavior):
            obj.clear_trade_info()
        elif (HANDLE_WITH_TICK == behavior):
            obj.handle_with_tick(close)

        elif (REFRESH_TICK_PROFIT == behavior):
            return obj.refresh_tick_profit()

        elif (CLEAR_STATE == behavior):
            obj.clear_states()

        elif (GET_STATE == behavior):
            return obj.get_status()

        elif (HANDLE_WITH_TICK_BOLLING == behavior):
            obj.handle_with_tick_bolling(close)


def daily_trade(date, filename, list, static_min_handler):

    _daily_df = pd.read_csv(_file_path + filename, low_memory=False, memory_map=True)
    print (filename)

    k_save = d_save = 0
    new_time_save = datetime.strptime("84500", "%H%M%S")
    buy_sell_save = 0

    highest = 0
    lowest = 999999
    found = ema_count = 0

    closePrice = 0
    checkNight = 0

    endTime = datetime.strptime("134500", "%H%M%S")

    # timeCheck = dayStart

    for index, row in _daily_df.iterrows():

        ts = datetime.strptime(str(row[TIME]).zfill(6), "%H%M%S")

        static_min_handler.static_tick(date, ts, row[PRICE])

        lastClose = row[PRICE]

    static_min_handler.static_min(endTime, lastClose)

    assign_worker(list, FORCE_CLOSE, lastClose, 0, 0, 0, 0)
    profit = assign_worker(list, REFRESH_TICK_PROFIT, 0, 0, 0, 0, 0)
    if (profit != sys.maxsize):
        static_min_handler.assigned_result(date.strftime("%Y%m%d"), profit, ts.strftime("%H%M%S"))

    static_min_handler.clear_daily()

strategist_list = []

strategist_list.append(Strategist_Min_Both_Multi.Strategist())

for obj in strategist_list:

    # 15m kline5
    # obj.add_strategist(30, 50, 1, 1, 3, 4, 0, 0, 1)
    # obj.add_strategist(30, 50, 1, 1, 0, 1, 3, 0, 1)
    # obj.add_strategist(50, 50, 1, 1, 3, 2, 1, 0, 1)
    # obj.add_strategist(70, 50, 1, 1, 2, 5, 0, 0, 1)
    # obj.add_strategist(30, 50, 1, 1, 3, 0, 1, 1, 1)
    # obj.add_strategist(50, 70, 1, 1, 4, 0, 1, 1, 1)
    # obj.add_strategist(30, 70, 1, 1, 2, 5, 1, 1, 1)
    # obj.add_strategist(70, 170, 1, 1, 0, 6, 2, 1, 1)
    # obj.add_strategist(50, 190, 1, 1, 0, 6, 2, 0, 1)
    # obj.add_strategist(90, 90, 1, 1, 4, 2, 1, 0, 1)
    # obj.add_strategist(70, 190, 1, 1, 1, 5, 1, 1, 1)
    # obj.add_strategist(50, 270, 1, 1, 4, 3, 0, 1, 1)
    # obj.add_strategist(50, 170, 1, 1, 3, 0, 0, 1, 1)
    # obj.add_strategist(70, 50, 1, 1, 2, 4, 1, 0, 1)
    # obj.add_strategist(90, 110, 1, 1, 1, 3, 2, 0, 1)
    # obj.add_strategist(70, 90, 1, 1, 2, 2, 2, 0, 1)
    # obj.add_strategist(10, 90, 1, 1, 2, 4, 0, 1, 1)
    # obj.add_strategist(90, 70, 1, 1, 2, 3, 0, 1, 1)
    # obj.add_strategist(70, 70, 1, 1, 2, 2, 0, 0, 1)
    # obj.add_strategist(30, 90, 1, 1, 1, 4, 0, 0, 1)
    # obj.add_strategist(30, 130, 1, 1, 0, 5, 0, 0, 1)
    # obj.add_strategist(90, 190, 1, 1, 1, 6, 0, 1, 1)
    # obj.add_strategist(70, 110, 1, 1, 0, 1, 3, 1, 1)

    # obj.add_strategist(90, 110, 1, 1, 3, 1, 2, 1, 2)
    # obj.add_strategist(90, 210, 1, 1, 5, 1, 1, 1, 2)
    # obj.add_strategist(90, 70, 1, 1, 3, 4, 1, 0, 2)
    # obj.add_strategist(90, 210, 1, 1, 0, 3, 2, 0, 2)
    # obj.add_strategist(70, 70, 1, 1, 0, 4, 2, 0, 2)
    # obj.add_strategist(50, 50, 1, 1, 1, 4, 2, 0, 2)
    # obj.add_strategist(30, 70, 1, 1, 1, 1, 4, 1, 2)
    # obj.add_strategist(50, 250, 1, 1, 3, 6, 1, 0, 2)
    # obj.add_strategist(90, 230, 1, 1, 3, 6, 1, 1, 2)
    # obj.add_strategist(90, 90, 1, 1, 0, 5, 1, 0, 2)
    # obj.add_strategist(30, 170, 1, 1, 0, 3, 2, 1, 2)
    # obj.add_strategist(10, 190, 1, 1, 4, 1, 1, 0, 2)
    # obj.add_strategist(70, 50, 1, 1, 0, 5, 2, 0, 2)
    # obj.add_strategist(50, 70, 1, 1, 3, 0, 2, 1, 2)
    # obj.add_strategist(90, 70, 1, 1, 1, 6, 1, 1, 2)
    # obj.add_strategist(50, 130, 1, 1, 6, 6, 1, 0, 2)
    # obj.add_strategist(90, 250, 1, 1, 3, 1, 1, 0, 2)
    # obj.add_strategist(90, 250, 1, 1, 1, 0, 3, 1, 2)
    # obj.add_strategist(90, 50, 1, 1, 1, 3, 3, 0, 2)
    # obj.add_strategist(70, 50, 1, 1, 2, 2, 1, 1, 2)
    # obj.add_strategist(30, 170, 1, 1, 3, 1, 0, 1, 2)
    # obj.add_strategist(90, 170, 1, 1, 1, 2, 2, 1, 2)
    # obj.add_strategist(90, 270, 1, 1, 1, 0, 3, 0, 2)
    # obj.add_strategist(50, 270, 1, 1, 1, 2, 2, 0, 2)
    
    
    # obj.add_strategist(70, 70, 1, 1, 2, 2, 0, 0, 1)
    # obj.add_strategist(30, 90, 1, 1, 1, 4, 0, 0, 1)
    # obj.add_strategist(90, 190, 1, 1, 1, 6, 0, 1, 1)
    # obj.add_strategist(70, 190, 1, 1, 1, 5, 1, 1, 1)
    # obj.add_strategist(90, 70, 1, 1, 2, 3, 0, 1, 1)
    # obj.add_strategist(50, 190, 1, 1, 0, 6, 2, 0, 1)
    # obj.add_strategist(90, 110, 1, 1, 1, 3, 2, 0, 1)
    # obj.add_strategist(70, 50, 1, 1, 2, 4, 1, 0, 1)
    # obj.add_strategist(30, 130, 1, 1, 0, 5, 0, 0, 1)
    # obj.add_strategist(50, 50, 1, 1, 3, 2, 1, 0, 1)
    # obj.add_strategist(30, 50, 1, 1, 3, 0, 1, 1, 1)
    # obj.add_strategist(50, 170, 1, 1, 3, 0, 0, 1, 1)
    # obj.add_strategist(10, 90, 1, 1, 2, 4, 0, 1, 1)
    # obj.add_strategist(30, 50, 1, 1, 0, 1, 3, 0, 1)
    # obj.add_strategist(90, 90, 1, 1, 4, 2, 1, 0, 1)
    # obj.add_strategist(70, 110, 1, 1, 0, 1, 3, 1, 1)
    # obj.add_strategist(30, 70, 1, 1, 2, 5, 1, 1, 1)
    # obj.add_strategist(70, 90, 1, 1, 2, 2, 2, 0, 1)
    # obj.add_strategist(70, 50, 1, 1, 2, 5, 0, 0, 1)
    # obj.add_strategist(70, 170, 1, 1, 0, 6, 2, 1, 1)
    # obj.add_strategist(50, 270, 1, 1, 4, 3, 0, 1, 1)
    # obj.add_strategist(30, 50, 1, 1, 3, 4, 0, 0, 1)
    # obj.add_strategist(50, 70, 1, 1, 4, 0, 1, 1, 1)

    # obj.add_strategist(70, 50, 1, 1, 2, 2, 1, 1, 2)
    # obj.add_strategist(50, 270, 1, 1, 1, 2, 2, 0, 2)
    # obj.add_strategist(90, 250, 1, 1, 3, 1, 1, 0, 2)
    # obj.add_strategist(90, 170, 1, 1, 1, 2, 2, 1, 2)
    # obj.add_strategist(30, 170, 1, 1, 3, 1, 0, 1, 2)
    # obj.add_strategist(90, 90, 1, 1, 0, 5, 1, 0, 2)
    # obj.add_strategist(30, 170, 1, 1, 0, 3, 2, 1, 2)
    # obj.add_strategist(90, 50, 1, 1, 1, 3, 3, 0, 2)
    # obj.add_strategist(90, 270, 1, 1, 1, 0, 3, 0, 2)
    # obj.add_strategist(90, 210, 1, 1, 0, 3, 2, 0, 2)
    # obj.add_strategist(90, 250, 1, 1, 1, 0, 3, 1, 2)
    # obj.add_strategist(90, 230, 1, 1, 3, 6, 1, 1, 2)
    # obj.add_strategist(90, 70, 1, 1, 1, 6, 1, 1, 2)
    # obj.add_strategist(70, 70, 1, 1, 0, 4, 2, 0, 2)
    # obj.add_strategist(10, 190, 1, 1, 4, 1, 1, 0, 2)
    # obj.add_strategist(70, 50, 1, 1, 0, 5, 2, 0, 2)
    # obj.add_strategist(50, 130, 1, 1, 6, 6, 1, 0, 2)
    # obj.add_strategist(90, 110, 1, 1, 3, 1, 2, 1, 2)
    # obj.add_strategist(90, 70, 1, 1, 3, 4, 1, 0, 2)
    # obj.add_strategist(50, 70, 1, 1, 3, 0, 2, 1, 2)
    # obj.add_strategist(90, 210, 1, 1, 5, 1, 1, 1, 2)
    # obj.add_strategist(50, 50, 1, 1, 1, 4, 2, 0, 2)
    # obj.add_strategist(50, 250, 1, 1, 3, 6, 1, 0, 2)
    # obj.add_strategist(30, 70, 1, 1, 1, 1, 4, 1, 2)

    obj.add_strategist(40 ,50, 1, 1, 3, 0, 1, 1, 1)
    obj.add_strategist(100, 210, 0, 1, 0, 2, 3, 0, 2)
    obj.add_strategist(100, 110, 1, 1, 4, 2, 1, 0, 1)
    obj.add_strategist(180, 210, 1, 1, 0, 5, 1, 1, 1)
    obj.add_strategist(120, 50, 1, 1, 3, 2, 1, 1, 1)



date_loop = Start_Date
delta = timedelta(days=1)

static_min_handler = Static_Min.Static_Min(strategist_list, Result, 1)

while date_loop <= End_Date:
    # print(date_loop)

    filename = datetime.strftime(date_loop, '%Y%m%d') + '_TX.csv'
    if not os.path.isfile(_file_path + filename):
        date_loop += delta
        continue

    daily_trade(date_loop, filename, strategist_list, static_min_handler)

    date_loop += delta

    # break

for obj in strategist_list:
    
    obj.settlement()        


dfLine = pd.DataFrame(Result)
dfLine.to_csv('static_min_trader/' + str(year) + '_test_one_trader_report_15m_kline.csv', index=False, encoding='utf_8_sig');


