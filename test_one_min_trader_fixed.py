import pandas as pd
import sys
import os
import numpy as np
import math
from datetime import datetime, timedelta, tzinfo, timedelta
from dateutil.relativedelta import relativedelta
import Strategist_Min
import Strategist_Min_Sell
import talib
import Static_Min

DATE = '成交日期'
CONTRACT = '商品代號'
EXPIRED_MONTH = '到期月份(週別)'
TIME = '成交時間'
PRICE = '成交價格'
VOLUME = '成交數量(B+S)'

_file_path = '../../CSVParser/daily_trading_details/dest_reduce/'
# _file_path = '../daily_trading_details/dest_reduce/'

HANDLE_WITH_STATIC = 1
FORCE_CLOSE = 2
SETTLEMENT = 3
REPORT = 4
CLEAR_TRADE_INFO = 5
HANDLE_WITH_TICK = 6
HANDLE_WITH_TICK_REF_STATIC = 7
HANDLE_WITH_MOVE_TICK = 8
HANDLE_WITH_STATIC_ALL_TICK = 9
STATIC_FORCE_CLOSE = 10
HANDLE_WITH_ALL_TICK_SHIFT = 11
HANDLE_WITH_ALL_TICK_SHIFT_HIGH = 12
HANDLE_WITH_ALL_TICK_ONE_SHIFT_HIGH = 13
HANDLE_WITH_ALL_TICK_ONE_SHIFT_HIGH_LOW_LIMIT = 14
REFRESH_TICK_PROFIT = 15
CLEAR_STATE = 16
GET_STATE = 17
HANDLE_WITH_TICK_BOLLING = 18

NONE = 4
BUY_ASKING = 2
SELL_ASKING = 5
BUY_ON_INTEREST = 3
SELL_ON_INTEREST = 6


Result = {'date':[], 'profit':[], 'time':[], 'k':[], 'd':[], 'H_line':[], 'M_line':[], 'L_line':[], 'new_time':[], 'buy_sell':[]}
Start_Date = datetime(2021, 1, 1, 0, 0)
End_Date = datetime(2021, 1, 5, 0, 0)

highArray = []
lowArray = []
closeArray = []


def assign_worker(list, behavior, close, high, low, slowk, slowd):
    for obj in list:
        if (HANDLE_WITH_STATIC == behavior):
            obj.handle_with_static(close, high, low, slowk, slowd)
        elif (FORCE_CLOSE == behavior):
            obj.force_close(close)
        elif (SETTLEMENT == behavior):
            obj.settlement()
        elif (REPORT == behavior):
            obj.report()
        elif (CLEAR_TRADE_INFO == behavior):
            obj.clear_trade_info()
        elif (HANDLE_WITH_TICK == behavior):
            obj.handle_with_tick(close)

        elif (REFRESH_TICK_PROFIT == behavior):
            return obj.refresh_tick_profit()

        elif (CLEAR_STATE == behavior):
            obj.clear_states()

        elif (GET_STATE == behavior):
            return obj.get_status()

        elif (HANDLE_WITH_TICK_BOLLING == behavior):
            obj.handle_with_tick_bolling(close)


def daily_trade(date, filename, list, static_min_handler):

    _daily_df = pd.read_csv(_file_path + filename, low_memory=False, memory_map=True)
    print (filename)

    k_save = d_save = 0
    new_time_save = datetime.strptime("84500", "%H%M%S")
    buy_sell_save = 0

    highest = 0
    lowest = 999999
    found = ema_count = 0

    closePrice = 0
    checkNight = 0

    endTime = datetime.strptime("134500", "%H%M%S")

    # timeCheck = dayStart

    for index, row in _daily_df.iterrows():

        ts = datetime.strptime(str(row[TIME]).zfill(6), "%H%M%S")

        static_min_handler.static_tick(date, ts, row[PRICE])

        lastClose = row[PRICE]

    static_min_handler.static_min(endTime, lastClose)

    assign_worker(list, FORCE_CLOSE, lastClose, 0, 0, 0, 0)
    profit = assign_worker(list, REFRESH_TICK_PROFIT, 0, 0, 0, 0, 0)
    if (profit != sys.maxsize):
        static_min_handler.assigned_result(date.strftime("%Y%m%d"), profit, ts.strftime("%H%M%S"))

    # # assign_worker(list, STATIC_FORCE_CLOSE, closePrice, 0, 0)
    # assign_worker(list, CLEAR_STATE, 0, 0, 0, 0, 0)

    static_min_handler.clear_daily()

strategist_list = []

# strategist_list.append(Strategist_Min.Strategist())

# 15min

# strategist_list.append(Strategist_Min_Both.Strategist(30, 130, 0, 1, -18, 30, \
#                                                     75, 120, 0, 1, 5, 10))

# strategist_list.append(Strategist_Min_Both.Strategist(30, 130, 0, 1, -18, 30, \
#                                                     45, 145, 0, 1, 5, 10))

# strategist_list.append(Strategist_Min.Strategist(30, 130, 0, 1, -18, -18, -18, 30))
# strategist_list.append(Strategist_Min.Strategist(22, 62, 0, 1, -40, -40, -40, 70))


# 5min
# strategist_list.append(Strategist_Min_Both.Strategist(42, 92, 0, 1, -20, 80, \
#                                                     42, 92, 0, 1, 10, 0))
# 3
# strategist_list.append(Strategist_Min_Both.Strategist(30, 200, 0, 1, 0, 80, \
#                                                     20, 120, 0, 1, -10, 0))
# 4
# strategist_list.append(Strategist_Min_Both.Strategist(20, 200, 0, 1, -10, 80, \
#                                                     20, 120, 0, 1, -10, 0))



# 15m ema_kd
# strategist_list.append(Strategist_Min_Both.Strategist(70, 100, 0, 1, -10, 90, \
                                                     # 70, 160, 1, 1, 0, 10))
# strategist_list.append(Strategist_Min.Strategist(70, 100, 0, 1, -10, -10, -10, 90, 0))

# strategist_list.append(Strategist_Multi.Strategist())


# for obj in strategist_list:

#     obj.add_strategist(70, 140, 0, 1, -100, 30)
#     obj.add_strategist(70, 120, 1, 1, 20, 40)
#     obj.add_strategist(70, 180, 0, 1, 10, 50)
#     obj.add_strategist(40, 180, 0, 1, 60, 60)
#     obj.add_strategist(60, 180, 0, 1, 80, 70)
#     obj.add_strategist(70, 100, 1, 1, 30, 80)
#     obj.add_strategist(70, 0, 1, 0, 20, 90)

# 15m bolling
# strategist_list.append(Strategist_Min_Both.Strategist(70, 100, 1, 1, 0, 0, \
#                                                      40, 190, 0, 1, 10, 0))
# strategist_list.append(Strategist_Min.Strategist(60, 100, 0, 1, 0, 0, 0, 0))

# 15m bolling_ema
# strategist_list.append(Strategist_Min_Both.Strategist(50, 180, 0, 1, -10, 80, 2, \
#                                                      70, 160, 1, 1, 0, 10, 1))
# 15m bolling_ema2
# strategist_list.append(Strategist_Min_Both.Strategist(70, 100, 0, 1, -10, 90, 2, \
#                                                      70, 160, 1, 1, 0, 10, 1))

# 15m bolling_ema_fix
# strategist_list.append(Strategist_Min_Both.Strategist(60, 180, 0, 1, -30, 70, 2, \
#                                                      70, 160, 1, 1, -10, 10, 1))

# 15m bolling_ema_fix2
# strategist_list.append(Strategist_Min_Both.Strategist(30, 180, 0, 1, -40, 50, 2, \
#                                                      70, 180, 1, 1, 10, 10, 1))

# 15m bolling_ema_fix
# strategist_list.append(Strategist_Min_Both.Strategist(60, 160, 0, 1, -30, 60, 0, \
#                                                      70, 160, 1, 1, -20, 10, 0))

# 15m kline
strategist_list.append(Strategist_Min.Strategist(100, 170, 0, 1, 0, 0, 0, 0, 0, \
                                                     3, 1, 1, 1, 0))


date_loop = Start_Date
delta = timedelta(days=1)

static_min_handler = Static_Min.Static_Min(strategist_list, Result, 1)

while date_loop <= End_Date:
    # print(date_loop)

    filename = datetime.strftime(date_loop, '%Y%m%d') + '_TX.csv'
    if not os.path.isfile(_file_path + filename):
        date_loop += delta
        continue

    daily_trade(date_loop, filename, strategist_list, static_min_handler)

    date_loop += delta

    # break

for obj in strategist_list:
    
    obj.settlement()        


dfLine = pd.DataFrame(Result)
dfLine.to_csv('static_min_trader/' + 'test_one_trader_report_15m_kline.csv', index=False, encoding='utf_8_sig');


