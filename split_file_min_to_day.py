import pandas as pd
import sys
import os
import numpy as np
import math

from datetime import datetime, timedelta, tzinfo, timedelta
from dateutil.relativedelta import relativedelta

from dask import dataframe as dd
import csv


year = 2012

read_file = 'static_min_trader/' + str(year) + '_close_static_15min_trader_kline.csv'


with open(read_file, 'r', encoding="utf-8-sig") as read_obj:
    csv_dict_reader = csv.DictReader(read_obj)

    date_loop = ''

    for row in csv_dict_reader:

        if (row['date'] != date_loop):
            date_loop = row['date']

            save_file_path = 'static_min_trader/split/days/' + row['date'] + '_split_file.csv'

            with open(save_file_path, 'w', newline='') as fd:
                header = 'date,profit,stop_loss,stop_profit,move_loss,do_stop_profit,KLine_low_rz,KLine_body_rz,KLine_upper_rz,KLine_trade,buy_sell\n'
                fd.write(header)

        save_string = row['date'] + ',' + row['profit'] + ',' + row['stop_loss'] + ',' + row['stop_profit'] + ',' + row['move_loss'] + ',' + \
                    row['do_stop_profit'] + ',' + row['KLine_low_rz'] + ',' + row['KLine_body_rz'] + ',' + row['KLine_upper_rz'] + \
                    ',' + row['KLine_trade'] + ',' + row['buy_sell']

        with open(save_file_path, 'a', newline='') as fd:
            # writer = csv.DictWriter(fd, fieldnames=row)
            fd.write(save_string + '\n')
