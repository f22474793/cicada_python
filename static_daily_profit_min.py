import pandas as pd
import sys
import os
import numpy as np
import math

from datetime import datetime, timedelta, tzinfo, timedelta
from dateutil.relativedelta import relativedelta

from dask import dataframe as dd
import csv

class Profit_Counter:

    def __init__(self, year, stop_loss, stop_profit, move_loss, do_stop_profit, KLine_low_rz, KLine_body_rz, KLine_upper_rz, KLine_trade, buy_sell, last_thres):

        self.stop_loss = stop_loss
        self.stop_profit = stop_profit
        self.move_loss = move_loss
        self.do_stop_profit = do_stop_profit
        self.KLine_low_rz = KLine_low_rz
        self.KLine_body_rz = KLine_body_rz
        self.KLine_upper_rz = KLine_upper_rz
        self.KLine_trade = KLine_trade
        self.buy_sell = buy_sell

        self.total_profit = 0

        self.trade_count = 0
        self.win_count = 0
        self.loss_count = 0

        self.win_profit = 0
        self.loss_profit = 0

        self.start = 0

        self.last_thres = last_thres
        self.last_profit = 0

        self.load_file_path = 'static_min_trader/split/' + str(year) + '/' + str(self.stop_loss) + '_' + str(self.stop_profit) + '_' + str(self.move_loss) + \
            '_' + str(self.do_stop_profit) + '_' + str(self.KLine_low_rz) + '_' + str(self.KLine_body_rz) + '_' + str(self.KLine_upper_rz) + \
            '_' + str(self.KLine_trade) + '_' + str(self.buy_sell) + \
            '_' + str(year) + '_split_file.csv'


    def static_data(self, profit):
        
        if (0 == profit):
            return

        self.trade_count += 1
        self.total_profit += profit

        if (profit > 0): 
            self.win_profit += profit
            self.win_count += 1
        else:
            self.loss_profit += profit
            self.loss_count += 1


    def record_profit(self, buy_sell):

        if (buy_sell != self.buy_sell):
            return

        with open(self.load_file_path, 'r', encoding="utf-8-sig") as read_obj:
            csv_dict_reader = csv.DictReader(read_obj)

            for row in csv_dict_reader:

                real_profit = int(float(row['profit']))

                if (0 == self.start):
                    self.last_profit = real_profit
                    self.start = 1
                    continue

                if (self.last_profit < self.last_thres):
                    self.static_data(real_profit)

                self.last_profit = real_profit


    def get_info(self):
        return self.stop_loss, self.stop_profit, self.move_loss, self.do_stop_profit, \
            self.KLine_low_rz, self.KLine_body_rz, self.KLine_upper_rz, self.KLine_trade, \
            self.buy_sell, self.last_thres, \
            self.total_profit, self.trade_count, self.win_profit, self.win_count, self.loss_profit, self.loss_count

    def clear_profit(self):

        self.total_profit = 0

        self.trade_count = 0
        self.win_count = 0
        self.loss_count = 0

        self.win_profit = 0
        self.loss_profit = 0


year = 2021

Start_Date = datetime(year, 1, 1, 0, 0)
End_Date = datetime(year + 1, 1, 1, 0, 0)

date_loop = Start_Date  
delta = timedelta(days=1)

record_delta = 1
record_month = Start_Date.month

df = dd.read_csv('static_min_trader/' + str(year) + '_close_static_15min_trader_kline.csv')

print ('read csv done')

profit_counter_buy = []
profit_counter_sell = []

# Kline
for buy_sell in range(1, 3, 1):
    for stop_loss in range(20, 200, 20):
        for KLine_low_rz in range(0, 6, 1):
            for KLine_body_rz in range(0, 6, 1):
                for KLine_upper_rz in range(0, 6, 1):
                    for KLine_trade in range(0, 2, 1):
                        for do_stop_profit in range (0, 2, 1):
                            if (1 == do_stop_profit):
                                for move_loss in range(0, 2, 1):
                                    for stop_profit in range(50, 320, 20):
                                        if (1 == buy_sell):
                                            profit_counter_buy.append(Profit_Counter(year, stop_loss, stop_profit, move_loss, do_stop_profit, \
                                                KLine_low_rz, KLine_body_rz, KLine_upper_rz, KLine_trade, buy_sell, 9999999))
                                        else:
                                            profit_counter_sell.append(Profit_Counter(year, stop_loss, stop_profit, move_loss, do_stop_profit, \
                                                KLine_low_rz, KLine_body_rz, KLine_upper_rz, KLine_trade, buy_sell, 9999999))

# profit_counter_buy.append(Profit_Counter(20, 50, 0, 1, \
#                                                 0, 0, 0, 0, 1, 9999999))
# profit_counter_sell.append(Profit_Counter(20, 50, 0, 1, \
#                                                 0, 0, 0, 0, 2, 9999999))

# profit_counter.append(Profit_Counter(10, 0, 0, 0, 45, 1, 9999999))


save_file_path = 'static_min_trader/' + str(year) + '_simulate_profit_year_thres.csv'

Result = {  'stop_loss':[], 'stop_profit':[], 'move_loss':[], 'do_stop_profit':[], \
            'KLine_low_rz':[], 'KLine_body_rz':[], 'KLine_upper_rz':[], 'KLine_trade':[], 'buy_sell':[], 'last_thres':[], \
            'total_profit':[], 'trade_count':[], 'profit_average':[], 'win_ratio':[], \
            'win_profit':[], 'win_count':[], 'win_average':[], \
            'loss_profit':[], 'loss_count':[], 'loss_average':[]}

dfLine = pd.DataFrame(Result)
dfLine.to_csv(save_file_path, index=False, encoding='utf_8_sig');


def record_result(obj, Result, save_file_path):

    stop_loss, stop_profit, move_loss, do_stop_profit, \
        KLine_low_rz, KLine_body_rz, KLine_upper_rz, KLine_trade, \
        buy_sell, last_thres, \
        total_profit, trade_count, win_profit, win_count, loss_profit, loss_count = obj.get_info()

    if (trade_count != 0):
        profit_average = total_profit / trade_count
        win_ratio = win_count/trade_count
    else:
        profit_average = 0
        win_ratio = 0

    if (win_count != 0):
        win_average = win_profit / win_count
    else:
        win_average = 0

    if (loss_count != 0):
        loss_average = loss_profit / loss_count
    else:
        loss_average = 0

    Result['stop_loss'].append(stop_loss)
    Result['stop_profit'].append(stop_profit)
    Result['move_loss'].append(move_loss)
    Result['do_stop_profit'].append(do_stop_profit)
    Result['KLine_low_rz'].append(KLine_low_rz)
    Result['KLine_body_rz'].append(KLine_body_rz)
    Result['KLine_upper_rz'].append(KLine_upper_rz)
    Result['KLine_trade'].append(KLine_trade)
    Result['buy_sell'].append(buy_sell)
    Result['last_thres'].append(last_thres)

    Result['total_profit'].append(total_profit)
    Result['trade_count'].append(trade_count)
    Result['profit_average'].append(profit_average)
    Result['win_ratio'].append(win_ratio)

    Result['win_profit'].append(win_profit)
    Result['win_count'].append(win_count)
    Result['win_average'].append(win_average)

    Result['loss_profit'].append(loss_profit)
    Result['loss_count'].append(loss_count)
    Result['loss_average'].append(loss_average)

    dfLine = pd.DataFrame(Result)
    dfLine.to_csv(save_file_path, mode='a', index=False, encoding='utf_8_sig', header=False);



for obj in profit_counter_buy:
    obj.record_profit(1)
    record_result(obj, Result, save_file_path)
    Result = {  'stop_loss':[], 'stop_profit':[], 'move_loss':[], 'do_stop_profit':[], \
            'KLine_low_rz':[], 'KLine_body_rz':[], 'KLine_upper_rz':[], 'KLine_trade':[], 'buy_sell':[], 'last_thres':[], \
            'total_profit':[], 'trade_count':[], 'profit_average':[], 'win_ratio':[], \
            'win_profit':[], 'win_count':[], 'win_average':[], \
            'loss_profit':[], 'loss_count':[], 'loss_average':[]}

for obj in profit_counter_sell:
    obj.record_profit(2)
    record_result(obj, Result, save_file_path)
    Result = {  'stop_loss':[], 'stop_profit':[], 'move_loss':[], 'do_stop_profit':[], \
            'KLine_low_rz':[], 'KLine_body_rz':[], 'KLine_upper_rz':[], 'KLine_trade':[], 'buy_sell':[], 'last_thres':[], \
            'total_profit':[], 'trade_count':[], 'profit_average':[], 'win_ratio':[], \
            'win_profit':[], 'win_count':[], 'win_average':[], \
            'loss_profit':[], 'loss_count':[], 'loss_average':[]}




